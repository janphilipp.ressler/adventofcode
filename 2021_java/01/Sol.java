import java.util.*;

public class Sol{
    public static void main(String... args){

        ArrayList<Integer> arr = new ArrayList<>();
        
        Scanner sc = new Scanner(System.in);

        while(sc.hasNextInt())
            arr.add(sc.nextInt());

        int sol1, sol2;
        sol1 = countIncreases(arr);

        ArrayList<Integer> brr = new ArrayList<>();
        for(int i=2; i < arr.size(); i++)
            brr.add(arr.get(i-2) + arr.get(i-1) + arr.get(i));

        sol2 = countIncreases(brr);

        System.out.println(sol1);
        System.out.println(sol2);

    }

    public static int countIncreases(ArrayList<Integer> a){
        int last, now;
        int counter = 0;

        for(int i=1; i < a.size(); i++){
            last = a.get(i-1);
            now = a.get(i);
            if(now > last)
                counter++;
        }
        return counter;
    }

}


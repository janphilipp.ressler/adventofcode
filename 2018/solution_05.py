# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read().strip()
print(data[:10])

# =============== part 1 ===============

def analyse(c: str):
  num = ord(c) - (ord('a') if c.islower() else ord('A'))
  upper = c.isupper()
  return upper, num

def react(s: str):
  counter = i = 0
  while i < len(s) - 1:
    curup, curnum = analyse(s[i])
    nextup, nextnum = analyse(s[i+1])

    if curup ^ nextup and curnum == nextnum:
      del s[i]
      del s[i]
      i = max(0, i-1)
      counter += 1
      # if i < 10:
      #   print(s[:13], i)
    else:
      i += 1
  return len(s)

d = list(data)
print(react(d))


# =============== part 2 ===============

ls = []
for i in range(ord('a'), ord('z')+1):
  c = chr(i)
  s = data.replace(c, '').replace(c.upper(), '')
  ls.append(react(list(s)))

print(min(ls))








# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
data = [(s, list(map(int, ls.split(',')))) for s, ls in (line.split() for line in data)]


# =============== part 1 ===============

def check(s: list, ls: list):
  # s = [len(list(g)) for k, g in it.groupby(s) if k == '#']
  s = list(filter(None, map(len, ''.join(s).split('.'))))
  return s == ls

def f1():
  res = 0
  for s, ls in data:
    s = list(s)
    unknowns = [i for i, c in enumerate(s) if c == '?']

    for mask in it.product([True, False], repeat = len(unknowns)):
      for idx, bit in zip(unknowns, mask):
        s[idx] = '#' if bit else '.'
      res += check(s, ls)
  return res


# use recursive function from part 2
def f1_alt():
  return sum(recursive_matching(s,tuple(ls)) for s, ls in data)


# =============== part 2 ===============

@ft.cache
def recursive_matching(s: str, ls: list):
  if ls == tuple():
    return '#' not in s
  if s == "": return 0

  match s[0]:
    case '.':
      return recursive_matching(s[1:], ls)
    case '#':
      l = ls[0]
      if len(s) < l: return 0
      pref, suff = s[:l], s[l:]
      if '.' in pref: return 0
      if suff != '' and suff[0] == '#': return 0
      return recursive_matching(suff[1:], ls[1:])
    case '?':
      return recursive_matching('#' + s[1:], ls) + recursive_matching(s[1:], ls)
    case _:
      print("Error")

def f2():
  return sum(recursive_matching('?'.join(5 * [s]), tuple(5 * ls)) for s, ls in data)

def dynpro(s: str, ls: tuple):
  s = s[::-1]
  ls = tuple(reversed(ls))
  dp = np.zeros((len(ls) + 1, len(s) + 1), dtype = int)
  dp[0,0] = 1

  for i in range(1, len(s) + 1):
    dp[0, i] = dp[0, i - 1] if s[i - 1] != '#' else 0

  for rem_lava_stone in range(1, len(ls) + 1):
    for i in range(1, len(s) + 1):
      res_a = dp[rem_lava_stone, i - 1]
      l = ls[rem_lava_stone - 1]
      tmp = i - 1 - l
      res_b = 0 if (i < l) or (('.' in s[i - 1: tmp: -1]) if tmp >= 0 else ('.' in s[i - 1::-1])) or (tmp >= 0 and s[tmp] == '#') else dp[rem_lava_stone - 1, max(0, tmp)]

      match s[i - 1]:
        case '.': r = res_a
        case '#': r = res_b
        case '?': r = res_a + res_b
      dp[rem_lava_stone, i] = r
  return dp[-1,-1]


def f2_alt():
  return sum(dynpro('?'.join(5 * [s]), tuple(5 * ls)) for s, ls in data)

# =============== main ===============

def main():
  # print(f1())
  print(f1_alt())
  print(f2())
  print(f2_alt())


if __name__ == "__main__":
  main()



#include<bits/stdc++.h>
using namespace std;

const int MAXN = 5000 + 2;
typedef long long ll;

// ========== algorithm ==========
int counter = 1;

const ll INF = 1LL << 60;
struct edge {
  int to, rev;
  ll cap, flw;
};
vector<edge> edges[MAXN];
vector<int> ptr, dis, ec(MAXN);
int s, t, lim = 1;
void addedge(int v, int w, ll c, bool dir){
  ll rc = dir ? 0 : c;
  edges[v].push_back({w, ec[w]++, c, 0});
  edges[w].push_back({v, ec[v]++, rc, 0});
}
int bfs_din(bool last = false){
  dis.assign(MAXN, -1);
  dis[s] = 0;
  vector<int> q{s};
  for(int id = 0; id < ssize(q); id++){
    for(auto &[to, r, c, f] : edges[q[id]]){
      if(dis[to] == -1 && c >= lim + f){
        q.push_back(to),
          dis[to] = dis[q[id]] + 1;
      }
    }
  }
  if(last) return size(q);
  return dis[t] != -1;
}
ll dfs_din(int v = s, ll mf = INF){
  if(!mf || v == t) return mf;
  for(; ptr[v] < ec[v]; ptr[v]++){
    auto &[to, r, c, f] = edges[v][ptr[v]];
    if(dis[to] != dis[v] + 1) continue;
    if(ll tf = dfs_din(to, min(mf, c - f))){
      f += tf, edges[to][r].flw -= tf;
      return tf;
    }
  }
  return 0;
}
ll lastres = 0;
ll dinic(){
  ll flow = 0;
  // for(lim = 1 << 30; lim > 0; lim >>= 1)
    while(bfs_din()){
      ptr.assign(MAXN, 0);
      while(ll tf = dfs_din()) flow += tf;
    }
  ll tmp = bfs_din(true);
  lastres = tmp * (counter - 1 - tmp);

  for(int i = 0; i < MAXN; i++){
      for(auto &[a,b,c,f]: edges[i]){
          f = 0;
      }
  }
  return flow;
}
// ========== algorithm ==========

map<string, int> id;

int main(){
    // parsing
    string line, from, to;
    while(getline(cin, line)){
        istringstream ss(line);
        ss >> from;
        from = from.substr(0, size(from) - 1);
        if(not id.contains(from)) id[from] = counter++;
        while(ss >> to){
            if(not id.contains(to)) id[to] = counter++;
            addedge(id[from], id[to], 1, false);
        }
    }
    
    // actual solution
    ll themin = INF, res = 0;
    ll tmp;
    s = 1;
    for(int i = 2; i < counter; i++){
        t = i;
        tmp = dinic();
        if(tmp == themin){
            res = max(res, lastres);
        }
        if(tmp < themin){
            res = lastres;
            themin = tmp;
        }
    }
    cout << res << endl;
}


# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read()

instructions, nodes = data.split("\n\n")
instructions = instructions.strip()

def parseNode(line: str):
  k, v = line.split("=")
  v = tuple(map(str.strip, v.strip(" ()").split(",")))
  return k.strip(), {"L": v[0], "R": v[1]}

nodes = {k: v for k, v in map(parseNode, nodes.splitlines())}

# =============== part 1 ===============

dostep = lambda n, d: nodes[n][d]

def f1():
  START = "AAA"
  END = "ZZZ"

  node = START
  for i, direc in enumerate(it.cycle(instructions), 1):
    node = dostep(node, direc)
    if node == END:
      return i

print(f1())


# =============== part 2 ===============

def find_length(node: str):
  for i, direc in enumerate(it.cycle(instructions), 1):
    node = dostep(node, direc)
    if node.endswith('Z'):
      return i


def f2():
  start_nodes = filter(lambda s: s.endswith('A'), nodes.keys())
  # end_nodes = filter(lambda s: s.endswith('Z'), nodes.keys())

  return lcm(*[find_length(n) for n in start_nodes])


print(f2())





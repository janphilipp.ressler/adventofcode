# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

from pprint import pprint

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().split("\n\n")
data = [l.split(":") for l in data]

seeds = list(map(int, data[0][1].split()))

maps = []
for name, l in data[1:]:
  l = [s.split() for s in l.strip().splitlines()]
  l = [{"from": int(f), "to": int(t), "range": int(r)} for t, f, r in l]
  maps.append((name.strip(), l))


# =============== part 1 ===============

def map_once(num: int, m: list):
  count = 0
  for d in m:
    if d["from"] <= num and num < d["from"] + d["range"]:
      count += 1
      res = num - d["from"] + d["to"]
  if count == 0:
    res = num
  assert(count <= 1)
  return res

def map_completely(seed: int):
  num = seed
  for name, m in maps:
    num = map_once(num, m)
  return num

def f1():
  return min(map_completely(seed) for seed in seeds)

print(f1())


# =============== part 2 ===============

def interval_decomposition(seedpair: tuple, m: list):
  m.sort(key = lambda d: d["from"])
  # start, end = seedpair[0], seedpair[0] + seedpair[1] - 1
  start, end = seedpair

  intervals = []
  for d in m:
    if end < start:
      break
    if start < d["from"]:
      s, e = start, min(end, d["from"] - 1)
      intervals.append((s, e))
      start = e + 1
    if end < start:
      break
    if d["from"] <= start and start < d["from"] + d["range"]:
      s, e = start, min(end, d["from"] + d["range"] - 1)
      intervals.append((s - d["from"] + d["to"], e - d["from"] + d["to"]))
      start = e + 1
  if not end < start:
    intervals.append((start, end))

  # print(seedpair, "=>", intervals)
  return intervals


def f2():
  intervals = [(a, a + b - 1) for a, b in zip(seeds[::2], seeds[1::2])]

  for name, m in maps:
    intervals_new = []
    for p in intervals:
      intervals_new.extend(interval_decomposition(p, m))
    intervals = intervals_new
    intervals.sort(key = lambda t: t[0])
    # optional connecting, dunno how to do this rn
    # print(len(intervals))
  return intervals[0][0]


print(f2())




# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

H = len(data)
W = len(data[0])

# =============== part 1 ===============

digits = set("0123456789")
no_symbols = set('.') | digits

def check_for_symbols(h: int, a: int, b: int):
  ha = max(h - 1, 0)
  hb = min(h + 1, H - 1)
  a = max(a - 1, 0)
  b = min(b + 1, W - 1)

  s = set()
  for i in range(ha, hb + 1):
    s.update(data[i][a : b+1])
  s = s.difference(no_symbols)
  return len(s) > 0


def f1(data):
  res = 0
  for h, line in enumerate(data):
    readingDigits = False
    for j, c in enumerate(line):
      if not readingDigits and c in digits:
        readingDigits = True
        start = j
      elif readingDigits and c not in digits:
        end = j - 1
        readingDigits = False
        if check_for_symbols(h, start, end):
          res += int(line[start : end + 1])
    if readingDigits:
      end = len(line) - 1
      if check_for_symbols(h, start, end):
        res += int(line[start : end + 1])
  return res

print(f1(data))

def f1_alt(data):
  numbers = []
  for h, line in enumerate(data):
    readingDigits = False
    for j, c in enumerate(line):
      if not readingDigits and c in digits:
        readingDigits = True
        start = j
      elif readingDigits and c not in digits:
        end = j - 1
        readingDigits = False
        numbers.append((h, start, end, int(line[start : end + 1])))
    if readingDigits:
      end = len(line) - 1
      numbers.append((h, start, end, int(line[start : end + 1])))
  return sum(num for h, a, b, num in numbers if check_for_symbols(h, a, b))

print(f1_alt(data))

# =============== part 2 ===============


def f2(data):
  # find all numbers
  numbers = []
  for h, line in enumerate(data):
    readingDigits = False
    for j, c in enumerate(line):
      if not readingDigits and c in digits:
        readingDigits = True
        start = j
      elif readingDigits and c not in digits:
        end = j - 1
        readingDigits = False
        numbers.append((h, start, end, int(line[start : end + 1])))
    if readingDigits:
      end = len(line) - 1
      numbers.append((h, start, end, int(line[start : end + 1])))

  # find all gears
  gears = []
  for h, line in enumerate(data):
    for j, c in enumerate(line):
      if data[h][j] == "*":
        gears.append((h, j))

  # for all gears count nearby numbers and calculate the result
  res = 0
  for i, j in gears:
    c = 0
    nums = []
    for h, a, b, num in numbers:
      if i in [h - 1, h, h + 1] and j in range(a - 1, b + 2):
        c += 1
        nums.append(num)
    if c == 2:
      res += prod(nums)

  return res

print(f2(data))






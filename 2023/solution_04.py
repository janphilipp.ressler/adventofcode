# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

cleandata = []
for i, line in enumerate(data):
  _, x = line.split(":")
  cardnums, winnums = x.strip().split("|")
  cardnums = cardnums.strip().split()
  winnums = winnums.strip().split()
  cleandata.append((i + 1, list(map(int,cardnums)), list(map(int, winnums))))

# =============== part 1 ===============

def f1(data):
  ls = [len(set(cs) & set(ws)) for i, cs, ws in data]
  return sum(2 ** (l - 1) for l in ls if l)
    
print(f1(cleandata))
    

# =============== part 2 ===============

numcards = len(cleandata)

def f2(data):
  cards = np.ones(numcards, dtype = int)
  for i, (_, cs, ws) in enumerate(data):
    l = len(set(cs) & set(ws))
    cards[i + 1 : i + 1 + l] += cards[i]
  return cards.sum()

print(f2(cleandata))




# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
data = [list(map(int, l.split())) for l in data]


# =============== part 1 ===============

def extrapolate(ls: list):
  if all(x == 0 for x in ls):
    return 0
  newls = [y - x for x, y in zip(ls, ls[1:])]
  return ls[-1] + extrapolate(newls)


def f1():
  return sum(extrapolate(ls) for ls in data)

print(f1())

# # one liner
# print(sum((ext:=lambda ls: 0 if all(x == 0 for x in ls) else ls[-1] + ext([y - x for x, y in zip(ls, ls[1:])]))(ls) for ls in [list(map(int, l.split())) for l in open(0).read().splitlines()]))

# =============== part 2 ===============

def f2():
  return sum(extrapolate(ls[::-1]) for ls in data)

print(f2())



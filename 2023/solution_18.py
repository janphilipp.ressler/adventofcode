# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
data = [(d, int(n), c.strip("()#")) for d, n, c in map(str.split, data)]


# =============== part 1 ===============

dirs = {
  'R': 1,
  'L': -1,
  'D': 1j,
  'U': -1j
}

def f1():
  s = 0

  holes = [s]
  reals, imags = [],[]
  lt, rt = 0, 0

  for d, n, _ in data:
    for _ in range(n):
      s += dirs[d]
      holes.append(s)
      reals.append(s.real)
      imags.append(s.imag)
      lt += d == 'L'
      rt += d == 'R'
  # print(lt, rt)

  minr, maxr = min(reals) - 1, max(reals) + 1
  mini, maxi = min(imags) - 1, max(imags) + 1
  
  holes = set(holes)

  used = set()
  q = queue.Queue()

  q.put((minr + mini * 1j))
  used.add(minr + mini * 1j)

  while not q.empty():
    x = q.get()
    for d in dirs.values():
      n = x + d
      if n in holes:
        continue
      if not minr <= n.real <= maxr or not mini <= n.imag <= maxi:
        continue
      if n not in used:
        q.put(n)
        used.add(n)

  gsize = (maxr - minr + 1) * (maxi - mini + 1)

  return int(gsize) - len(used)


# =============== part 2 ===============

# currently not correct
def f2():
  curx, cury = 0, 0
  area = 0

  for _, _, s in data[::-1]:
    n, d = int(s[:-1],16), (1j ** int(s[-1]))
    move = n * d
    curx += int(move.real)
    cury += int(move.imag)
    # cur += n * d

    match d:
      case 1:
        # area -= int(cur.real) * int(cur.imag - 1)
        area -= curx * (cury - 1)
      case -1:
        # area -= int(cur.real - 1) * int(cur.imag)
        area -= (curx - 1) * cury
      case 1j:
        # area += int(cur.real) * int(cur.imag)
        area += curx * cury
      case -1j:
        # area += (int(cur.real) - 1) * int(cur.imag - 1)
        area += (curx - 1) * (cury - 1)
      case _:
        print("error")

  # print(cur)
  # print(curx, cury)
  return abs(area)


def f2_alt():
  cur = 0
  area = 0
  circum = 0

  for _, _, s in data:
    n, d = int(s[:-1],16), (1j ** int(s[-1]))
    diff = n * d
    area += int(diff.real * cur.imag)
    circum += int(abs(diff))
    cur += diff

  return abs(area) + circum // 2 + 1


# =============== main ===============

def main():
  print(f1())
  print(f2())
  print(f2_alt())

if __name__ == "__main__":
  main()


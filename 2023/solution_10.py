# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

H = len(data)
W = len(data[0])

# =============== part 1 ===============

North, East, South, West = 0, 1, 2, 3

coming_from = {
  North: (0, 1),
  South: (0, -1),
  East: (-1, 0),
  West: (1, 0)
}

di = {
  ('|', North): North,
  ('|', South): South,
  ('-', West): West,
  ('-', East): East,
  ('L', North): West,
  ('L', East): South,
  ('J', North): East,
  ('J', West): South,
  ('7', South): East,
  ('7', West): North,
  ('F', South): West,
  ('F', East): North
}

path = []

def f1():
  for i, l in enumerate(data):
    if 'S' in l:
      y = i
      x = l.find('S')
      path.append((y,x))

  # current position
  cx, cy, cfrom, steps = x, y, None, 0

  # check where we can go
  nodir = True
  if nodir and y > 0 and (data[cy - 1][cx], South) in di.keys():
    nodir = False
    cfrom = South
    cy += -1
  if nodir and y < H - 1 and (data[cy + 1][cx], North) in di.keys():
    nodir = False
    cfrom = North
    cy += 1
  if nodir and x > 0 and (data[cy][cx - 1], East) in di.keys():
    nodir = False
    cfrom = East
    cx += -1
  if nodir and x < W - 1 and (data[cy][cx + 1], West) in di.keys():
    nodir = False
    cfrom = West
    cx += 1

  steps += 1
  cfrom = di[(data[cy][cx], cfrom)]
  path.append((cy,cx))

  while True:
    # print(f"cpos = ({cx}, {cy}), {cfrom=}, {steps=} - {data[cy][cx]}")
    dx, dy = coming_from[cfrom]
    cx += dx
    cy += dy
    steps += 1
    if data[cy][cx] == 'S':
      break
    cfrom = di[(data[cy][cx], cfrom)]
    path.append((cy,cx))
  # print(f"cpos = ({cx}, {cy}), {cfrom=}, {steps=} - {data[cy][cx]}")

  return steps // 2

print(f1())

# =============== part 2 ===============

from queue import SimpleQueue

def show(m):
  for row in m:
    for c in row:
      display = {
        0: '_',
        1: '#',
        2: 'O'
      }
      print(display[c], end = '')
    print()

def f2():
  # double the map
  newH = 2 * H + 1
  newW = 2 * W + 1
  newmap = np.zeros((newH, newW), dtype = int)

  # double the path coords accordingly
  path.append(path[0])
  pathids = np.array(path) * 2 + 1

  # set the doubled path tiles
  for r, c in pathids:
    newmap[r, c] = 1
  # show(newmap)

  # set the tiles in between the doubled path tiles
  for (r, c), (rr, cc) in zip(pathids, pathids[1:]):
    r = (r + rr) // 2
    c = (c + cc) // 2
    newmap[r, c] = 1
  # show(newmap)

  # do floodfill
  q = SimpleQueue()
  q.put((0,0))
  newmap[0, 0] = 2

  dirs = [(1, 0), (-1, 0), (0, 1), (0, -1)]
  while not q.empty():
    y, x = q.get()
    for oy, ox in [(y + dy, x + dx) for dy, dx in dirs]:
      if 0 <= oy < newH and 0 <= ox < newW and newmap[oy, ox] == 0:
        newmap[oy, ox] = 2
        q.put((oy, ox))
  # show(newmap)

  # count all those tiles where there are 4 zeros
  return np.sum(newmap[1::2, 1::2] == 0)

print(f2())






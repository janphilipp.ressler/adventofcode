# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
H = len(data)
W = len(data[0])

for i, row in enumerate(data):
  for j, c in enumerate(row):
    if data[i][j] == "S":
      Sx, Sy = j, i


# =============== part 1 ===============

def f1(maxstep = 64):
  g = np.zeros((H, W), dtype = int)

  q = queue.SimpleQueue()
  q.put((Sx, Sy, 0))

  while not q.empty():
    x, y, s = q.get()
    for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
      ox = x + dx
      oy = y + dy
      if not 0 <= ox < W or not 0 <= oy < H:
        continue
      if data[oy][ox] == "#":
        continue
      if s <= maxstep and g[oy,ox] < s + 1:
        g[oy, ox] = s + 1
        q.put((ox, oy, s + 1))

  return (g == maxstep).sum()


# =============== part 2 ===============

def f2():
  pass


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


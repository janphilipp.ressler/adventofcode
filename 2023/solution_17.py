# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
H = len(data)
W = len(data[0])
grid = np.zeros((H, W), dtype = int)
for i, row in enumerate(data):
  for j, heatloss in enumerate(row):
    grid[i,j] = int(data[i][j])


# =============== part 1 ===============

INF = 1_000_000_000_000

NORTH, EAST, SOUTH, WEST = 0, 1, 2, 3
dirs = {
  NORTH: (-1, 0),
  EAST: (0, 1),
  SOUTH: (1, 0),
  WEST: (0, -1)
}

def dijkstra(maxstraight = 3, minstraight = 1):
  dp = np.ones((H, W, 4), dtype = int) * INF
  used = np.zeros((H, W, 4), dtype = int)
  # bt = dict()

  q = queue.PriorityQueue()
  q.put((0, NORTH, 0, 0))
  q.put((0, EAST, 0, 0))
  q.put((0, SOUTH, 0, 0))
  q.put((0, WEST, 0, 0))

  dp[0,0, :] = 0

  while(not q.empty()):
    dis, nodir, y, x = q.get()
    used[y, x, nodir] = 1
    if dp[y, x, nodir] < dis:
      # print("ohoh")
      continue
    for direc in [(nodir + 1) % 4, (nodir + 3) % 4]:
      dy, dx = dirs[direc]
      for i in range(minstraight, maxstraight + 1):
        oy, ox = y + i * dy, x + i * dx
        if not 0 <= oy < H or not 0 <= ox < W: continue
        if used[oy, ox, direc]: continue
        s = sum(grid[oy - j * dy, ox - j * dx] for j in range(i))
        # print(oy, ox, s)
        if dp[y, x, nodir] + s < dp[oy, ox, direc]:
          dp[oy, ox, direc] = dp[y, x, nodir] + s
          q.put((dp[oy, ox, direc], direc, oy, ox))
          # bt[(oy, ox, direc)] = (y, x, nodir)

  # print(dp.min(axis = 2))

  # # for the sake of debugging
  # def backtrack(by, bx, d):
  #   while (by, bx) != (0, 0):
  #     by, bx, d = bt[(by, bx, d)]
  #     print(by, bx, d, dp[by, bx, d])
  # backtrack(H - 1, W - 1, 1)
  # backtrack(6, 11, SOUTH)
  # backtrack(10, 12, NORTH)

  return dp[H-1,W-1,:].min()

def f1():
  return dijkstra()


# =============== part 2 ===============

def f2():
  return dijkstra(10, 4)


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


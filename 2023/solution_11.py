# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
H = len(data)
W = len(data[0])


# =============== part 1 ===============

def f1(expansionNum = 1):
  rows = set()
  cols = set()
  galaxies = list()
  for i, row in enumerate(data):
    for j, c in enumerate(row):
      if c == '#':
        rows.add(i)
        cols.add(j)
        galaxies.append((i,j))

  rows = set(range(H)) - rows
  cols = set(range(W)) - cols

  rowspace = np.zeros(H, dtype = int)
  rowspace[list(rows)] = expansionNum
  colspace = np.zeros(W, dtype = int)
  colspace[list(cols)] = expansionNum

  res = 0
  for (x, y), (xx, yy) in it.combinations(galaxies, 2):
    x, xx = min(x, xx), max(x, xx)
    y, yy = min(y, yy), max(y, yy)
    res += xx - x + yy - y + rowspace[x:xx].sum() + colspace[y:yy].sum()
  return res


# =============== part 2 ===============

def f2():
  return f1(expansionNum = 1_000_000 - 1)


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


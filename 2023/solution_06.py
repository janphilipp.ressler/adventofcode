# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
times, dists = data

times = list(map(int, times.split(":")[1].split()))
dists = list(map(int, dists.split(":")[1].split()))

data = times, dists


# =============== part 1 ===============

def f1():
  waycounts = []
  for i, t, d in zip(it.count(), times, dists):
    waycounts.append(sum(holdtime * (t - holdtime) > d for holdtime in range(t + 1)))
  return prod(waycounts)

print(f1())

# =============== part 2 ===============

def f2():
  time = int("".join(map(str, times)))
  dist = int("".join(map(str, dists)))
  return sum(holdtime * (time - holdtime) > dist for holdtime in range(time + 1))

print(f2())





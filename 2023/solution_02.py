# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
cleandata = []
for i, line in enumerate(data):
  _, l = line.split(": ")
  game = []
  for s in l.split(";"):
    # (r, g, b)
    d = {"red": 0, "green": 0, "blue": 0}
    for e in s.split(","):
      num, col = e.split()
      d[col.strip()] = int(num.strip())
    game.append((d["red"], d["green"], d["blue"]))
  cleandata.append((i + 1, game))


# =============== part 1 ===============

redth = 12
greenth = 13
blueth = 14

def f1(data):
  res = 0
  for id, game in data:
    for r, g, b in game:
      if r > redth or g > greenth or b > blueth:
        break
    else:
      res += id
  return res

print(f1(cleandata))

# one liner
print(sum(id for id, game in cleandata if all(r <= 12 and g <= 13 and b <= 14 for r,g,b in game)))


# =============== part 2 ===============

def f2(data):
  res = 0
  for id, game in data:
    minred, mingreen, minblue = map(max, zip(*game))
    res += minred * mingreen * minblue
  return res

print(f2(cleandata))

# one liner
print(sum(prod(map(max, zip(*game))) for _, game in cleandata))



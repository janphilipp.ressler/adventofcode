# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()


# =============== part 1 ===============

def f1():
  pass


# =============== part 2 ===============

def f2():
  pass


# =============== main ===============

def main():
  print(f1())
  # print(f2())

if __name__ == "__main__":
  main()


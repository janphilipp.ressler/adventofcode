# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
H = len(data)
W = len(data[0])

transpose = lambda t: [''.join(c) for c in zip(*t)] # transpose the data
rotate90 = lambda t: list(reversed(transpose(t)))

cols = transpose(data)


# =============== part 1 ===============

def f1(table: list = cols):
  res = 0
  for col in table:
    betweens = [bet.count('O') for bet in col.split('#')]
    col = "#" + col
    solids = [i for i, r in enumerate(col) if r == '#']
    res += (H + 1) * col.count('O')
    res -= sum(sum(range(s + 1, s + 1 + num)) for s, num in zip(solids, betweens))
  return res

def f1_alt(table = cols):
  return count(tilt_north(table))


# =============== part 2 ===============

def tilt_north(cols: List[str]):
  table = []
  for col in cols:
    betweens = [bet.count('O') for bet in col.split('#')]
    col = '#' + col + '#'
    solids = [i for i, r in enumerate(col) if r == '#']
    solidspaces = [sec - fir for fir, sec in zip(solids, solids[1:])]
    ls = [num * "O" + (sp - num - 1) * "." + "#" for sp, num in zip(solidspaces, betweens)]
    table.append("".join(ls)[:-1])
  return table

def show_north_table(table: list):
  for row in transpose(table):
    print(row)
  print()

def do_cycle(table: List[str]):
  for i in range(4):
    # tilt to north
    table = tilt_north(table)
    # rotate by 90˚
    table = rotate90(table)
  # show_north_table(table)
  return table

BIGNUM = 1_000_000_000
BN = 1000000000

def count(table: List[str]):
  return sum(row.count('O') * i for i, row in enumerate(reversed(transpose(table)), 1))

def f2():
  table = cols
  slow = do_cycle(table)
  fast = do_cycle(do_cycle(table))
  slow_i = 1

  cmp = lambda ot, nt: all(ocol == ncol for ocol, ncol in zip(ot, nt))

  while not cmp(fast, slow):
    fast = do_cycle(do_cycle(fast))
    slow = do_cycle(slow)
    slow_i += 1

  cycle = do_cycle(slow)
  cycle_i = 1
  while not cmp(cycle, slow):
    cycle = do_cycle(cycle)
    cycle_i += 1

  endstate = BN - slow_i
  endstate %= cycle_i
  for i in range(endstate):
    slow = do_cycle(slow)

  return count(slow)


# =============== main ===============

def main():
  print(f1())
  print(f1_alt())
  print(f2())

if __name__ == "__main__":
  main()



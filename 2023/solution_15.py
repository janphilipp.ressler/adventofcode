# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().strip()


# =============== part 1 ===============

def hash256(s: str):
  res = 0
  for v in map(ord, s):
    res += v
    res *= 17
    res %= 256
  return res

def f1():
  return sum(hash256(s) for s in data.split(','))


# =============== part 2 ===============

def f2():
  boxes = []
  for _ in range(256): boxes.append([])
  focal = dict()
  for token in data.split(','):
    if '=' in token:
      label, num = token.split('=')
      labelhash = hash256(label)
      num = int(num)
      if label not in boxes[labelhash]:
        boxes[labelhash].append(label)
      focal[label] = num
    elif '-' in token:
      label = token.strip('-')
      labelhash = hash256(label)
      box = boxes[labelhash]
      if label in box:
        del box[box.index(label)]
    else:
      print("ERROR")

  return sum(boxID * sum(lense_pos * focal[label] for lense_pos, label in enumerate(box, 1)) for boxID, box in enumerate(boxes, 1))


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


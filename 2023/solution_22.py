# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

class brick():
  # instance variables
  id: int

  lx: int
  hx: int
  ly: int
  hy: int
  lz: int
  hz: int

  supporting: list
  supportedby: list

  # class variables
  idcounter = 0

  minx = inf
  miny = inf
  minz = inf
  maxx = 0
  maxy = 0
  maxz = 0


  def __init__(self, line: str):
    # init id in list data
    self.id = brick.idcounter
    brick.idcounter += 1

    # coordindates
    A, B = line.split("~")
    A = list(map(int, A.strip().split(',')))
    B = list(map(int, B.strip().split(',')))
    
    self.lx, self.hx = min(A[0], B[0]), max(A[0], B[0])
    self.ly, self.hy = min(A[1], B[1]), max(A[1], B[1])
    self.lz, self.hz = min(A[2], B[2]), max(A[2], B[2])

    brick.minx = min(brick.minx, self.lx)
    brick.miny = min(brick.miny, self.ly)
    brick.minz = min(brick.minz, self.lz)

    brick.maxx = max(brick.maxx, self.hx)
    brick.maxy = max(brick.maxy, self.hy)
    brick.maxz = max(brick.maxz, self.hz)

    # init dependency nodes
    self.supporting = set()
    self.supportedby = set()

data = {b.id: b for b in map(brick, data)}

# =============== part 1 ===============

def f1():
  order = sorted(data.values(), key = lambda b: b.lz)
  
  # grid is holding tuples of two values
  # the first represents the current height at this x-y-coordinate
  # the second the id of the top brick
  grid = np.zeros((10, 10), dtype = (int,2))
  grid[:,:,1] = -1

  for b in order:
    spacebelow = grid[b.lx : b.hx + 1, b.ly : b.hy + 1]
    maxheight = spacebelow[:,:,0].max()
    fallheight = b.lz - maxheight - 1
    b.lz -= fallheight
    b.hz -= fallheight

    for suppbrick in set(spacebelow[spacebelow[:,:,0] == maxheight][:,1]):
      if suppbrick == -1: continue
      suppbrick = data[suppbrick]
      b.supportedby.add(suppbrick)
      suppbrick.supporting.add(b)
    
    spacebelow[:,:,0] = b.hz
    spacebelow[:,:,1] = b.id
    # print(grid[:,:,0])

  res = sum(all(len(ob.supportedby) >= 2 for ob in b.supporting) for b in data.values())
  return res


# =============== part 2 ===============

def chain(b: brick, ls: list):
  res = 0
  q = queue.SimpleQueue()
  q.put(b)
  while not q.empty():
    b = q.get()
    for ob in b.supporting:
      ls[ob.id] -= 1
      if ls[ob.id] == 0:
        q.put(ob)
        res += 1
  return res

def f2():
  ls = [len(b.supportedby) for b in data.values()]
  return sum(chain(b, ls.copy()) for b in data.values())


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()


# =============== part 1 ===============

# res = 0
# for l in data:
#   ls = [c for c in l if c.isdigit()]
#   res += int(ls[0] + ls[-1])

# print(res)

def f1(data):
  return sum(int(ls[0]+ls[-1]) for ls in [list(filter(str.isdigit,l)) for l in data])

print(f1(data))

# one liner
# print(sum(int((ls:=list(filter(str.isdigit,l)))[0]+ls[-1])for l in open(0).read().splitlines()))

# print(sum(int((ls:=[c for c in l if c.isdigit()])[0]+ls[-1])for l in open(0).read().splitlines()))

# print(sum(int(ls[0]+ls[-1])for ls in([c for c in l if c.isdigit()]for l in open(0).read().splitlines())))

# print(sum(map(lambda ls:int(ls[0]+ls[-1]),[list(filter(str.isdigit,l))for l in open(0).read().splitlines()])))

# =============== part 2 ===============

words = [(0, 'zero'), (1, 'one'), (2, 'two'), (3, 'three'), (4, 'four'), (5, 'five'), (6, 'six'), (7, 'seven'), (8, 'eight'), (9, 'nine')]

def f2(data):
  # because no number word is completely included in one another we can simply
  # replace w by w + d + w to preserve the prefixes and suffixes
  def rep(s):
    for d, w in words:
      s = s.replace(w, w+str(d)+w)
    return s

  return f1([rep(line) for line in data])


print(f2(data))



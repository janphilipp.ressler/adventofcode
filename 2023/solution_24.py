# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

class line:
  x: int
  y: int
  z: int

  vx: int
  vy: int
  vz: int

  def __init__(self, line: str):
    pos, vel = line.split("@")
    self.x, self.y, self.z = [int(v.strip()) for v in pos.split(",")]
    self.vx, self.vy, self.vz = [int(v.strip()) for v in vel.split(",")]

data = list(map(line, data))

# =============== part 1 ===============

EPSI = 1e-9

MINNUM = 200_000_000_000_000
MAXNUM = 400_000_000_000_000

# MINNUM = 7
# MAXNUM = 27

def intersect2(l1: line, l2: line):
  # check for parallels:
  alph = l1.vx / l2.vx
  beta = l1.vy / l2.vy

  if abs(alph - beta) < EPSI:
    # lines are parallel
    # l2.x = l1.x + l1.vx * mu
    mu = (l2.x - l1.x) / l1.vx
    estimated_ypos = l1.y + l1.vy * mu
    if abs(l2.y - estimated_ypos) < EPSI:
      # identical
      return True
    else:
      # disjoint
      return False

  # so now that they are not parallel,
  # solve the well defined linear equation system
  # x + mu * vx == xx + nu * vxx
  # mu * vx - nu * vxx == xx - x
  # mu * vy - nu * vyy == yy - y
  
  A = np.array([
    [l1.vx, -l2.vx],
    [l1.vy, -l2.vy]
  ])
  b = [l2.x - l1.x, l2.y - l1.y]
  x = np.linalg.solve(A, b)
  mu, nu = x
  x = l1.x + l1.vx * mu
  y = l1.y + l1.vy * mu

  if mu < 0 or nu < 0:
    return False
  return all(MINNUM <= v <= MAXNUM for v in [x, y])


def f1():
  return sum(intersect2(*t) for t in it.combinations(data, 2))


# =============== part 2 ===============

def f2():
  pass


# =============== main ===============

def main():
  print(f1())
  # print(f2())

if __name__ == "__main__":
  main()


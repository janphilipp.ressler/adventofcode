# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = [t.splitlines() for t in open(0).read().split("\n\n")]

data_matrix = []
for t in data:
  tab = np.zeros((len(t), len(t[0])), dtype = int)
  for i, row in enumerate(t):
    for j, c in enumerate(row):
      if c == "#":
        tab[i, j] = 1
  data_matrix.append(tab)


# =============== part 1 ===============

def find_reflection(tab: np.ndarray, without: int = 0):
  res = 0
  l = tab.shape[0]
  for i in range(1, l):
    if i == without:
      continue
    if i <= l - i:
      # print(tab[:i])
      # print(tab[i: 2*i][::-1])
      if np.all(tab[:i] == tab[i:2*i][::-1]):
        res = i
    else:
      # print(tab[i:])
      # print(tab[i-(l-i):i][::-1])
      if np.all(tab[i:] == tab[i-(l-i):i][::-1]):
        res = i
  return res

def f1():
  # assume that there is only one reflection axis
  # => there will never be one horizontal and one vertical
  return sum(100 * find_reflection(tab) + find_reflection(tab.T) for tab in data_matrix)


# =============== part 2 ===============

def f2():
  res = 0
  for tab in data_matrix:
    # print(tab)
    href = find_reflection(tab)
    vref = find_reflection(tab.T)
    r, c = tab.shape
    for i, j in it.product(range(r), range(c)):
      # print("flip", i, j)
      cop = tab.copy()
      cop[i,j] = not cop[i,j]
      res += 100 * find_reflection(cop, href) + find_reflection(cop.T, vref)
  return res // 2


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()



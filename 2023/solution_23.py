# =============== imports ===============
import re, copy
# import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
H = len(data)
W = len(data[0])

dirs = [(1,0), (-1, 0), (0, 1), (0, -1)]
slopes = {
  ">": dirs[2],
  "<": dirs[3],
  "v": dirs[0],
  "^": dirs[1]
}

START = (0, 1)
END = (H - 1, W - 2)

def neighbours(i, j):
  return [(i + di, j + dj) for di, dj in dirs if 0 <= i + di < H and 0 <= j + dj < W and data[i + di][j + dj] != "#"]

def get_intersections():
  ls = [START, END]
  for i, j in it.product(range(H), range(W)):
    if data[i][j] != "#":
      if len(neighbours(i, j)) > 2:
        assert data[i][j] == "."
        ls.append((i,j))
  return ls

inters = get_intersections()

# =============== part 1 ===============

cntvar = 0
grid = list()
for _ in range(H):
  grid.append([0] * W)

def find_adj_list(u, care_for_slopes = True):
  global cntvar
  ls = []
  # for n in neighbours(*u):
    # do bfs until next inter v
  cntvar += 1
  q = queue.SimpleQueue()
  q.put((u, 0))
  grid[u[0]][u[1]] = cntvar
  while not q.empty():
    n, d = q.get()
    if n != u and n in inters:
      ls.append((n, d))
      continue
      # break
    for v in neighbours(*n):
      if grid[v[0]][v[1]] < cntvar:
        grid[v[0]][v[1]] = cntvar
        # check if v is a one way tile
        ground = data[v[0]][v[1]]
        if care_for_slopes and ground != ".":
          di, dj = slopes[ground]
          if v[0] + di == n[0] and v[1] + dj == n[1]:
            continue
        q.put((v, d + 1))
  return ls

def f1(part2 = False):
  # global grid
  # grid = np.zeros((H, W), dtype = int)

  adj_list = dict()
  for u in inters:
    adj_list[u] = find_adj_list(u, care_for_slopes = not part2)

  # @ft.cache
  def rec_longest_path(u: tuple, vis: frozenset):
    if u == END:
      return 0
    candidates = [dd + rec_longest_path(v, vis | {v}) for v, dd in adj_list[u] if v not in vis]
    return max(candidates, default = -10000)

  # print(*adj_list.items(), sep = "\n")
  return rec_longest_path(START, frozenset(START))


# =============== part 2 ===============

def f2():
  return f1(True)


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


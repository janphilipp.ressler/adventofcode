# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()


# =============== part 1 ===============

class camelcard():
  hand: str
  bid: int
  type_name: str

  def __init__(self, hand: str, bid: str):
    self.hand = hand
    self.bid = int(bid)
    self.type_name = self.type()

  typenum = {
    "five_of_a_kind": 7,
    "four_of_a_kind": 6,
    "full_house": 5,
    "three_of_a_kind": 4,
    "two_pair": 3,
    "one_pair": 2,
    "high_card": 1
  }
  def type(self):
    # print("Im a Normi")
    ls = [(num, card) for card, num in col.Counter(self.hand).items()]
    ls.sort(reverse = True)
    match ls[0]:
      case [5, card]:
        return "five_of_a_kind"
      case [4, card]:
        return "four_of_a_kind"
      case [3, card]:
        if ls[1][0] == 2:
          return "full_house"
        else:
          return "three_of_a_kind"
      case [2, card]:
        if ls[1][0] == 2:
          return "two_pair"
        else:
          return "one_pair"
      case _:
        return "high_card"


  cardvalue = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2
  }
  def key(self):
    return camelcard.typenum[self.type_name], *[camelcard.cardvalue[c] for c in self.hand]

  def __repr__(self):
    return self.hand
  # def __str__(self):
  #   return self.hand


def f1():
  ls = [camelcard(*l.split()) for l in data]
  ls.sort(key = camelcard.key)
  return sum(i * c.bid for i, c in zip(it.count(1), ls))


print(f1())


# =============== part 2 ===============

class camelcard_Joker(camelcard):
  cardvalue = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 1,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2
  }

  hand_with_J_replaced: str
  def type(self):
    # print("Im a Joker")
    if 'J' not in self.hand:
      hand_with_J_replaced = self.hand
      return super(camelcard_Joker, self).type()

    # there is a J in the hand
    # its enough to replace all J's by the same card since we the best hand is among these
    poss_hands = [self.hand.replace('J', repCard) for repCard in camelcard_Joker.cardvalue.keys()]
    ls = [camelcard(hand, 0) for hand in poss_hands]
    ls.sort(key = camelcard.key, reverse = True)
    hand_with_J_replaced = ls[0].hand
    return ls[0].type()

  def key(self):
    return camelcard.typenum[self.type_name], *[camelcard_Joker.cardvalue[c] for c in self.hand]


def f2():
  ls = [camelcard_Joker(*l.split()) for l in data]
  ls.sort(key = camelcard_Joker.key)
  return sum(i * c.bid for i, c in zip(it.count(1), ls))


print(f2())



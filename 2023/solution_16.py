# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()
H = len(data)
W = len(data[0])

# =============== part 1 ===============

dirs = {
  1: (0, 1),
  2: (1, 0),
  3: (0, -1),
  4: (-1, 0)
}

import queue

def energize(sy, sx, sdir):
  g = np.zeros((H, W, 5), dtype = int)

  def bfs(y, x, dir):
    if not 0 <= y < H or not 0 <= x < W:
      return
    # print(y, x, "Direction:", dirs[dir], "reading", data[y][x], data[y])
    g[y, x, 0] = 1
    if not g[y, x, dir]:
      g[y, x, dir] = 1
      match dir:
        case 1:
          match data[y][x]:
            case '.':
              d = [dir]
            case '/':
              d = [4]
            case '\\':
              d = [2]
            case '-':
              d = [dir]
            case '|':
              d = [2, 4]
        case 2:
          match data[y][x]:
            case '.':
              d = [dir]
            case '/':
              d = [3]
            case '\\':
              d = [1]
            case '-':
              d = [1, 3]
            case '|':
              d = [dir]
        case 3:
          match data[y][x]:
            case '.':
              d = [dir]
            case '/':
              d = [2]
            case '\\':
              d = [4]
            case '-':
              d = [dir]
            case '|':
              d = [2, 4]
        case 4:
          match data[y][x]:
            case '.':
              d = [dir]
            case '/':
              d = [1]
            case '\\':
              d = [3]
            case '-':
              d = [1, 3]
            case '|':
              d = [dir]

      for nd in d:
        dy, dx = dirs[nd]
        q.put((y + dy, x + dx, nd))

  q = queue.SimpleQueue()
  q.put((sy, sx, sdir))
  while not q.empty():
    bfs(*q.get())
  return g[:,:,0].sum()

def f1():
  return energize(0,0,1)


# =============== part 2 ===============

def f2():
  ls = []
  for i in range(H):
    ls.append(energize(i, 0, 1))
    ls.append(energize(i, W - 1, 3))
  for j in range(W):
    ls.append(energize(0, j, 2))
    ls.append(energize(H - 1, j, 4))
  return max(ls)

# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


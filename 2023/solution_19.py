# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

wfs, parts = open(0).read().split("\n\n")

workflows = dict()

for w in wfs.splitlines():
  name, rules = w.split("{")
  rules = rules.strip("}").split(",")
  ruleset = {cond : name for cond, name in [r.split(":") for r in rules[:-1]]}
  workflows[name] = (ruleset, rules[-1])

x = "X"
m = "M"
a = "A"
s = "S"
parts = [eval(p.replace("=", ":")) for p in parts.splitlines()]


# =============== part 1 ===============

def process(prt, wfname):
  if wfname == "A": return True
  if wfname == "R": return False

  rules, default = workflows[wfname]
  for cond, name in rules.items():
    for c in "xmas":
      cond = cond.replace(c, f"prt[{c}]")
    if eval(cond):
      return process(prt, name)
  return process(prt, default)

def f1():
  return sum(sum(p.values()) for p in parts if process(p, "in"))


# =============== part 2 ===============

def combis(part: dict):
  ls = [(part[c + "min"], part[c + "max"]) for c in "xmas"]
  return prod(maxi - mini + 1 for mini, maxi in ls if maxi >= mini)

def processall(part, wfname):
  if wfname == "A": return combis(part)
  if wfname == "R": return 0

  rules, default = workflows[wfname]
  res = 0
  for cond, name in rules.items():
    opart = part.copy()
    if "<" in cond:
      c, num = cond.split("<")
      opart[c + "max"] = int(num) - 1
      part[c + "min"] = int(num)
    else:
      c, num = cond.split(">")
      opart[c + "min"] = int(num) + 1
      part[c + "max"] = int(num)
    res += processall(opart, name)

  return res + processall(part, default)

def f2():
  part = dict()
  for c in "xmas":
    part[c + "min"] = 1
    part[c + "max"] = 4000
  return processall(part, "in")


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import queue
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

class module():
  module: str
  name: str
  outs: list
  state: dict
  got_low_pulse: int
  got_high_pulse: int

  def __init__(self, s: str):
    name, ls = s.split('->')
    if name.startswith("%"):
      self.module = "flipflop"
      self.name = name[1:].strip()
      self.state = 0
    elif name.startswith("&"):
      self.module = "conjunction"
      self.name = name[1:].strip()
      self.state = dict()
    else:
      self.module = "broadcaster"
      self.name = name.strip()
      self.state = None

    self.outs = ls.strip().replace(",", "").split()
    # self.state = {n : 0 for n in self.ls}
    self.got_low_pulse = 0
    self.got_high_pulse = 0

  def add_input(self, fr: str):
    if self.module != "conjunction": return
    self.state[fr] = 0

  def reset(self):
    self.got_low_pulse = 0
    self.got_high_pulse = 0
    match self.module:
      case "flipflop":
        self.state = 0
      case "conjunction":
        for k in self.state.keys():
          self.state[k] = 0
      case "broadcaster":
        pass
      case _:
        print("error", self.name)

  def propagate(self, pulse: int, fr: str):
    if self.module == "broadcaster":
      return list(zip(self.outs, it.repeat(pulse), it.repeat(self.name)))

    if self.module == "flipflop":
      if pulse == 1:
        return []
      else:
        self.state = not self.state
        return list(zip(self.outs, it.repeat(self.state), it.repeat(self.name)))

    if self.module == "conjunction":
      self.state[fr] = pulse
      send_pulse = not all(self.state.values())
      return list(zip(self.outs, it.repeat(send_pulse), it.repeat(self.name)))
      
modules = {m.name : m for m in map(module, data)}
modules["output"] = module("output ->") # in example there exists a "output"
modules["rx"] = module("rx ->")
for m in modules.values():
  for o in m.outs:
    modules[o].add_input(m.name)

# # show
# for m in modules.values():
#   print(m.name, m.module, m.outs, m.state)

# =============== part 1 ===============

def push_button(it: int = 0):
  counts = {0:0, 1:0}
  q = queue.SimpleQueue()
  q.put(("broadcaster", 0, "button"))

  while not q.empty():
    name, pulse, fr = q.get()
    counts[pulse] += 1
    # print(f"{fr} ({pulse}) -> {name}")
    ls = modules[name].propagate(pulse, fr)
    for e in ls: q.put(e)

    # relevant for part 2 only
    if it and not pulse and not modules[name].got_low_pulse:
      print(name, it, "low")
      modules[name].got_low_pulse = 1
    if it and pulse and not modules[name].got_high_pulse:
      print(name, it, "high")
      modules[name].got_high_pulse = 1
  return counts[0], counts[1]

def f1():
  for m in modules.values(): m.reset()
  lows, highs = 0, 0
  for _ in range(1000):
    # print("--- new round ---")
    ls, hs = push_button()
    lows += ls
    highs += hs
  print(lows, highs)
  return lows * highs


# =============== part 2 ===============

# multiply the low values of all inputs
# for the last conjunction before rx
# -- not automated yet
def f2():
  for m in modules.values(): m.reset()
  i = 0
  # while not modules["rx"].got_low_pulse:
  while not modules["rx"].got_low_pulse:
    i += 1
    push_button(i)
  return i


# =============== main ===============

def main():
  print(f1())
  print(f2())

if __name__ == "__main__":
  main()


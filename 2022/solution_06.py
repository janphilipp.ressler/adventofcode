# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read()


# =============== part 1 ===============

for i in range(4,len(data)):
  if len(set(data[i-4:i])) == 4:
    print(i)
    break

# =============== part 2 ===============

for i in range(14,len(data)):
  if len(set(data[i-14:i])) == 14:
    print(i)
    break





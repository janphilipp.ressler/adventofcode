# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
import pprint

# =============== preparation ===============

data = open(0).read().splitlines()

s = '$. '
for i in range(26):
  s += chr(ord('a') + i)

tokens = []
counter = 0
for line in data:
  if '$ cd ..' in line:
    tokens.append('],')
    counter -= 1
  elif '$ cd' in line:
    tokens.append('[')
    counter += 1
  else:
    line = line.strip(s)
    if len(line) > 0:
      tokens.append(line + ',')
for i in range(counter):
  tokens.append(']')

ls = eval(''.join(tokens).replace(',]', ']'))
# print(ls)

# =============== part 1 ===============

sizes = []
def cs(l):
  s = 0
  for e in l:
    s += e if type(e) == int else cs(e)
  sizes.append(s)
  return s

cs(ls)

r1 = sum([s for s in sizes if s <= 100000])
print(r1)

# =============== part 2 ===============

total = 70000000
used = max(sizes)
unused = total - used
needed = 30000000 - unused

deletable = [ s for s in sizes if s - needed >= 0 ]
r2 = min(deletable)
print(r2)





# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
cc = lambda s: eval('complex(' + s.replace('x','real').replace('y','imag') + ')')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = [ [cc(val.split(' at ')[-1]) for val in line.split(':')] for line in open(0).read().splitlines() ]

dist = lambda a, b: abs(a.real - b.real) + abs(a.imag - b.imag)
sens, beacs = map(list, zip(*data))

# print(sens, len(sens))
# print(beacs, len(beacs))

# =============== part 1 ===============

def unifiable(fst, snd):
  (a,b), (c,d) = fst, snd
  return not (c - b >= 2 or a - d >= 2)

def unifyInts(fst, snd):
  (a,b), (c,d) = fst, snd
  if not unifiable((a,b), (c,d)):
    return None
  if a <= c <= b and a <= d <= b:
    return (a,b)
  elif c <= a <= d and c <= b <= d:
    return (c,d)
  elif a <= c and b <= d and c - b <= 1:
    return (a,d)
  elif c <= a and d <= b and a - d <= 1:
    return (c,b)
  else:
    print('Error')
    print(a, b, c, d, c - b, a - d)

def unifyAllInts(ls: list):
  newls = []
  while len(ls) > 0:
    cur = ls[0]
    del ls[0]
    for i, (c,d) in enumerate(ls):
      if unifiable(cur, (c,d)):
        new = unifyInts(cur, (c,d))
        del ls[i]
        ls.append(new)
        # print('unified ', cur, (c,d), 'to', new)
        break
    else:
      newls.append(cur)
  return newls

row = 2000000
# row = 10
intervals = []
for s, b in data:
  d = dist(s,b)
  needed = abs(s.imag - row)
  start = s.real
  leftover = d - needed
  if leftover >= 0:
    intervals.append((start - leftover, start + leftover))

print(sum(int(b-a) for a,b in unifyAllInts(intervals)))

# =============== part 2 ===============

# for i in range(4000001):
#   row = i
#   intervals = []
#   for s, b in data:
#     d = dist(s,b)
#     needed = abs(s.imag - row)
#     start = s.real
#     leftover = d - needed
#     if leftover >= 0:
#       intervals.append((start - leftover, start + leftover))
#   # if i % 40000 == 0:
#   #   print(i // 40000 * 0.01)
#   l = unifyAllInts(sorted(intervals))
#   if len(l) > 1:
#     l.sort()
#     vals = [ int(b+1) for (a,b),(c,d) in zip(l, l[1:]) if b+1 == c-1 ]
#     vals = list(filter(lambda x: 0 <= x <= 4000000, vals))
#     for v in vals:
#       # print(v, row)
#       print(v * 4000000 + row)


# =============== part 2 - fast ===============

def findSensorPairsWithGap(data):
  pairs = []
  for (s,b), (os,ob) in it.combinations(data, 2):
    d = dist(s,os)
    bb = dist(s,b) + dist(os, ob)
    if d - bb == 2:
      # print(s, os, d - bb)
      pairs.append(((s,b), (os,ob)))
  # print(pairs)
  return pairs


def oneBehindSight(pair: tuple):
  f, s = pair
  f, s = (f,s) if dist(*f) < dist(*s) else (s,f)
  (sensor, ab), (b, bb) = f, s
  locs = set()
  l = list(range(int(dist(*f)) + 2))
  for x,y in it.product((-1,1), repeat=2):
     for a,b in zip(l, reversed(l)):
         # print(x * a + y * b * 1j)
         locs.add(sensor + x * a + y * b * 1j)
  return locs

pairs = findSensorPairsWithGap(data)
for uno, duo in it.combinations(pairs, 2):
  locs = oneBehindSight(uno) & oneBehindSight(duo)
  for l in locs:
    table = [ dist(s,l) <= dist(s,b) for s,b in data]
    # print(table, any(table))
    if not any(table):
      # print('WINNER')
      # print(l.real, l.imag)
      print(int(l.real * 4000000 + l.imag))












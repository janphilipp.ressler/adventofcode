# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = [eval('(' + line + ')') for line in open(0).read().splitlines() ]


# =============== part 1 ===============

def neighbours(cube):
  a,b,c = cube
  for i,j,k in [ (1,0,0), (-1,0,0), (0,1,0), (0,-1,0), (0,0,1), (0,0,-1) ]:
    yield (a + i, b + j, c + k)

# counter = 0
# for cube in data:
#   counter += 6
#   for n in neighbours(cube):
#     if n in data:
#       counter -= 1
# print(counter)

print(sum(n not in data for cube in data for n in neighbours(cube)))

# =============== part 2 ===============

aa, bb, cc = l =  list(zip(*data))

xmin, xmax = min(aa) - 1, max(aa) + 1
ymin, ymax = min(bb) - 1, max(bb) + 1
zmin, zmax = min(cc) - 1, max(cc) + 1

def boundcheck(cube):
  a, b, c = cube
  return xmin <= a <= xmax and ymin <= b <= ymax and zmin <= c <= zmax


start = (xmin, ymin, zmin)
vis = set()
vis.add(start)
q = col.deque(vis)

counter = 0
while len(q) > 0:
  for n in neighbours(q.popleft()):
    if n in vis:
      continue
    elif n in data:
      counter += 1
    elif boundcheck(n):
      vis.add(n)
      q.append(n)

print(counter)




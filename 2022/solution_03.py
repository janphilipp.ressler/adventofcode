# =============== imports ===============
import sys
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *


# =============== preparation ===============

inp = sys.stdin
data = inp.read().splitlines()

l = [ int(len(s)/2) for s in data ]

# =============== part 1 ===============
d = [ (set(s[:l]) , set(s[l:])) for s, l in zip(data, l)]
r = [ a & b for a, b in d]


def points(c):
  if c.islower():
    return ord(c) - ord('a') + 1
  else:
    return ord(c) - ord('A') + 27

a = []
for s in r: a.extend(s)
print(a)

r = [ points(c) for c in a]
# print(r)
print(sum(r))

# =============== part 2 ===============

res = 0
for i in range(0,len(data), 3):
  a = set(data[i])
  b = set(data[i+1])
  c = set(data[i+2])
  sol = a & b & c
  print(sol)
  for c in sol:
    res += points(c)

print(res)





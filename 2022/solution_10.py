# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read().splitlines()

data = [None if 'noop' in line else int(line.split()[1]) for line in data]

# =============== part 1 ===============

ths = [20, 60, 100, 140, 180, 220]
ls = []
def check(cycle, strength):
  if cycle in ths:
    # print(cycle, strength)
    ls.append(cycle * strength)

slist = []
cycle = 0
strength = 1
for e in data:
  cycle += 1
  slist.append(strength)
  check(cycle, strength)
  if e == None:
    continue
  else:
    cycle += 1
    slist.append(strength)
    check(cycle, strength)
    strength += e

# print(cycle)
print(sum(ls))

# =============== part 2 ===============

display = ''
for i in range(6):
  row = ''
  for j in range(40):
    sp = slist[40 * i + j]
    row += '##' if j in [sp-1, sp, sp+1] else '  '
  row += '\n'
  display += row

print(display)



# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

import queue
# =============== preparation ===============

data = open(0).read().split("\n\n")

class Monkey():
  id: int 
  items: queue.Queue
  op:str
  testby: int
  pos:int
  neg:int

  def __init__(self, s: str):
    pat = """\
Monkey (\d*):
  Starting items: (.*)
  Operation: new = (.*)
  Test: divisible by (\d*)
    If true: throw to monkey (\d*)
    If false: throw to monkey (\d*)\
"""
    m = re.match(pat, s)
    # print(m.groups())
    self.id, self.items, self.op, self.testby, self.pos, self.neg = m.groups()
    self.id = int(self.id)
    items = eval('[' + self.items + ']')
    self.items = queue.SimpleQueue()
    for i in items:
      self.items.put(i, block = False)
    self.testby = int(self.testby)
    self.pos, self.neg = int(self.pos), int(self.neg)
    self.num_inspections = 0

  def catch(self, item: int):
    self.items.put(item)

  def inspect(self):
    old = self.items.get()
    new = eval(self.op)
    self.num_inspections += 1
    return new // 3

  def inspect_2(self):
    old = self.items.get()
    new = eval(self.op)
    self.num_inspections += 1
    return new

  def __str__(self):
    a = f"I'm Monkey {self.id}"
    b = f"  My props are: {self.op}, div by {self.testby} ? {self.pos} : {self.neg}"
    c = f"  My items are: {self.items}"
    return '\n'.join([a,b,c])


# =============== part 1 ===============

mons = [Monkey(line) for line in data]

for i in range(20):
  for mon in mons:
    while not mon.items.empty():
      item = mon.inspect()
      newmonkeyid = mon.pos if item % mon.testby == 0 else mon.neg
      mons[newmonkeyid].catch(item)
    # print(mon.num_inspections)

ins = sorted(mon.num_inspections for mon in mons)
print(ins[-1] * ins[-2])

# =============== part 2 ===============

mons = [Monkey(line) for line in data]

kgv = prod(mon.testby for mon in mons)
rounds = 10000

for _ in range(rounds):
  for mon in mons:
    while not mon.items.empty():
      item = mon.inspect_2()
      item %= kgv
      newmonkeyid = mon.pos if item % mon.testby == 0 else mon.neg
      mons[newmonkeyid].catch(item)
    # print(mon.num_inspections)

ins = sorted(mon.num_inspections for mon in mons)
print(ins, kgv)
print(prod(ins[-2:]))



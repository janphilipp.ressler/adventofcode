# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read()
puzzle, moves = data.split("\n\n")

p = [''.join(col) for col in zip(*puzzle.splitlines())]
p = [''] + [ stack.strip()[-2::-1] for i, stack in enumerate(p) if i % 4 == 1 ]
p = [list(stack) for stack in p]

pat = r"move (\d+) from (\d+) to (\d+)"
m = re.compile(pat)
moves = [list(map(int, m.match(move).groups())) for move in moves.splitlines()]

print(puzzle)
refinedpuzzle = p
print(refinedpuzzle)
# print(moves)

# =============== part 1 ===============

p = copy.deepcopy(refinedpuzzle)

for num, a, b in moves:
  for _ in range(num):
    p[b].append(p[a].pop())

print(''.join([st[-1] for st in p if len(st) >= 1]))


# =============== part 2 ===============

p = copy.deepcopy(refinedpuzzle)
for num, a, b in moves:
  p[b].extend(p[a][-num:])
  p[a] = p[a][:-num]

print(''.join([st[-1] for st in p if len(st) >= 1]))





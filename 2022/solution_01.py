# =============== imports ===============
import sys
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
import math


# =============== preparation ===============

inp = sys.stdin
data = inp.read()


# =============== part 1 ===============

d = data.split("\n\n")
d = [sum(map(int, ls.splitlines())) for ls in d]
print(max(d)) 


# =============== part 2 ===============

# print(sorted(d))
r = sum(sorted(d)[-3:])
print(r)





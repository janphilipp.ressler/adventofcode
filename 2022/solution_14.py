# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== preparation ===============

class Point(NamedTuple):
  x: int
  y: int
  def __add__(self, o: 'Point'):
    return Point(self.x + o.x, self.y + o.y)
P = Point

data = [ [ Point(*map(int, t.split(','))) for t in l.split(' -> ')] for l in open(0).read().splitlines()]

blocks = set()
for seq in data:
  current = seq[0]
  for following in seq[1:]:
    c,f = current, following
    sx, ex = (c.x, f.x) if c.x <= f.x else (f.x, c.x)
    sy, ey = (c.y, f.y) if c.y <= f.y else (f.y, c.y)
    for i in range(sy, ey + 1):
      for j in range(sx, ex + 1):
        blocks.add(P(j,i))
    current = following
# print(blocks)

xs = [p.x for p in blocks]
ys = [p.y for p in blocks]
minx, maxx = min(xs), max(xs)
miny, maxy = min(ys), max(ys)
print(miny, maxy, minx, maxx)

# normalize block coords
def norm(p: Point):
  return P(p.x - minx, p.y)

walls = []
for b in blocks:
  walls.append(norm(b))

# =============== part 1 ===============

# init grid
grid = np.zeros((maxy + 1, maxx-minx +1), dtype = bool)
for w in walls:
  grid[w.y, w.x] = True
# print(grid)

def isInside(p: Point):
  by, bx = grid.shape
  return 0 <= p.x < bx and 0 <= p.y < by

def isFree(p: Point):
  return isInside(p) and not grid[p.y, p.x]

# possible next spots
a = P(0,1)
b = P(-1,1)
c = P(1,1)

counter = 0
restInside = True
while restInside:
  sand = norm(P(500,0))
  resting = False
  while not resting:
    if isFree(sand + a): sand += a
    elif isFree(sand + b): sand += b
    elif isFree(sand + c): sand += c
    elif not isInside(sand + a)\
      or not isInside(sand + b)\
      or not isInside(sand + c):
        restInside = False
        break
    else:
      resting = True
      grid[sand.y, sand.x] = True
      counter += 1

print(counter)

# =============== part 2 ===============

ground = maxy + 2
counter = 0
blocked = set(walls)

flowing = True
while flowing:
  sand = norm(P(500,0))
  resting = False
  while not resting:
    if (sand + a).y == ground:
      resting = True
      blocked.add(sand)
    else:
      if not sand+a in blocked:
        sand += a
      elif not sand+b in blocked:
        sand += b
      elif not sand+c in blocked:
        sand += c
      else:
        resting = True
        blocked.add(sand)
        if sand.y == 0:
          flowing = False
  counter += 1

print(counter)


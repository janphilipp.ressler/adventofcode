# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== preparation ===============

data = open(0).read().split('\n\n')
data = [ (eval(f), eval(s)) for f,s in (pair.splitlines() for pair in data) ]

# print(*data, sep = '\n=====\n')

# =============== part 1 ===============

def compare(a: Union[list,int], b: Union[list,int], debug = False):
  if debug: print(a, b, sep = ' <===> ')
  if list in (type(a), type(b)):
    if type(a) == type(b):
      # both are lists
      if len(a) == len(b) == 0:
        return 0
      if len(a) == 0:
        return -1
      if len(b) == 0:
        return 1
      if r := compare(a[0], b[0], debug):
        return r
      else:
        return compare(a[1:], b[1:], debug)
    else:
      # one is list, one not
      assert type(a) == int and type(b) == list or type(a) == list and type(b) == int
      if type(a) == list:
        return compare(a, [b], debug)
      else:
        return compare([a], b, debug)
  # both are ints
  assert type(a) == type(b) == int
  if a == b:
    return 0
  else:
    c = a - b
    r = c // abs(c)
    return r

 
ls = [ i for i, (a,b) in enumerate(data, start = 1) if compare(a,b, debug=False) < 0 ]
print(sum(ls))


# =============== part 2 ===============

# divider packages
a = [[2]]
b = [[6]]
ls = [a,b]

for f,s in data:
  ls.append(f)
  ls.append(s)

# generate key function for sorting
kf = ft.cmp_to_key(compare)

ls.sort(key = kf)
print( (ls.index(a) + 1) * (ls.index(b) + 1))




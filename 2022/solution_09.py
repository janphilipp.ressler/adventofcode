# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read().splitlines()
moves = ''
for l in data:
  d, n = l.split()
  moves += d * int(n)

# print(len(moves))

# =============== part 1 ===============

class Coord(col.namedtuple("Coord", ['x', 'y'])):
  def __add__(self, o):
    return Coord(self.x + o.x, self.y + o.y)

  def adja(self, o):
    return abs(self.x - o.x) <= 1 and abs(self.y - o.y) <= 1

  def wdintm(self, o): # where do I need to move?
    if self.adja(o):
      return Coord(0,0)
    dx = 1 if o.x > self.x else -1
    dy = 1 if o.y > self.y else -1
    if o.x == self.x: dx = 0
    if o.y == self.y: dy = 0
    return Coord(dx, dy)

h = Coord(0,0)
t = Coord(0,0)

pos = {t}
move = {'U': Coord(0,1), 'D': Coord(0, -1), 'L': Coord(-1,0), 'R': Coord(1,0) }
for m in moves:
  lasth = h
  h += move[m]
  if not t.adja(h):
    t = lasth
    pos.add(t)
    # print(t)

print(len(pos))


# =============== part 2 ===============

ropelen = 10
rope = []
for i in range(ropelen): rope.append(Coord(0,0))

pos = {rope[-1]}
for m in moves:
  rope[0] += move[m]
  for i in range(1, 10):
    rope[i] += rope[i].wdintm(rope[i-1])
  pos.add(rope[-1])

print(len(pos))



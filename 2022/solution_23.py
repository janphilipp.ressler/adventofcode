# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = [ complex(real, imag) for imag, line in enumerate(open(0).read().splitlines()) for real, char in enumerate(line) if char == '#' ]


# =============== part 1 ===============

adjs = [ -1-1j, -1j, 1-1j, -1, 1, -1+1j, 1j, 1+1j ]

propDirs = [ [ -1-1j, -1j, 1-1j ], [ -1+1j, 1j, 1+1j ], [ -1-1j, -1, -1+1j ], [ 1-1j, 1, 1+1j ] ]
def giveProposal(elves: set, elve: complex, proposalDirectionStartIndex: int):
  for i in range(4):
    propDir = ( proposalDirectionStartIndex + i ) % 4
    if all( (elve + adj) not in elves for adj in propDirs[propDir] ):
      return elve + propDirs[propDir][1]
  return None

def showElves(elves: set):
  reals, imags = zip( *[ (int(e.real), int(e.imag)) for e in elves ] )
  minr, maxr = min(reals), max(reals)
  mini, maxi = min(imags), max(imags)
  for j in range(mini, maxi + 1):
    for i in range(minr, maxr + 1):
      print('#' if complex(i,j) in elves else '_', end = '')
    print()

def simulateRound(elves: set, roundNo: int, show = False, returnMoving = False):
    relations = dict()
    props = set()
    dups = set()
    for e in elves:
      if all( e + adj not in elves for adj in adjs ):
        continue
      prop = giveProposal(elves, e, roundNo)
      if prop == None:
        continue
      if prop in dups:
        continue
      if prop in props:
        dups.add(prop)
        continue
      props.add(prop)
      relations[e] = prop

    relations = { e: p for e, p in relations.items() if p not in dups }

    newElves = set(relations.values())
    remainingElves = set(elves) - set(relations.keys())
    elves = newElves | remainingElves
    if show:
      showElves(elves)
    if returnMoving:
      return elves, len(newElves) > 0
    return elves

def part1(rounds = 10):
  elves = set(data.copy())
  for i in range(rounds):
    # print(propstart, len(elves))
    elves = simulateRound(elves, i)
    
  reals, imags = zip( *[ (e.real, e.imag) for e in elves ] )
  minr, maxr = min(reals), max(reals)
  mini, maxi = min(imags), max(imags)
  rectSpace = int((maxr - minr + 1) * (maxi - mini + 1))
  print(rectSpace - len(elves))

part1()


# =============== part 2 ===============

def part2():
  elves = set(data.copy())
  moving = True
  counter = 0
  while moving:
    elves, moving = simulateRound(elves, counter, returnMoving = True)
    counter += 1
  print(counter)

part2()


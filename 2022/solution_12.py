# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read().splitlines()
h,w = len(data), len(data[0])

# find start and end point
for i,r in enumerate(data):
  if 'S' in r:
    j = r.index('S')
    start = sx,sy = i,j
  if 'E' in r:
    j = r.index('E')
    end = ex,ey = i,j
print(start, end)

# parse relief into np array
relief = np.zeros((h,w))
for i, r in enumerate(data):
  for j, c in enumerate(r):
    relief[i, j] = ord(c)

relief[sx,sy] = ord('a')
relief[ex,ey] = ord('z')

# print(data)
# print(relief)

# =============== part 1 ===============

d = np.ones((h,w)) * (h*w)
q = col.deque()

d[sx, sy] = 0
q.appendleft((sx,sy))

while len(q) > 0:
  x,y = q.pop()
  if (x,y) == end:
    break
  for dx, dy in [(1,0), (-1,0), (0,1), (0,-1)]:
    ox, oy = x+dx, y+dy
    if 0 <= ox < h\
      and 0 <= oy < w\
      and relief[ox, oy] <= relief[x,y] + 1\
      and d[x,y] + 1 < d[ox, oy]:
      d[ox, oy] = d[x,y] + 1
      q.appendleft((ox, oy))
      # print(ox, oy, d[ox, oy])

# print(d)

print(h*w)
print(int(d[ex,ey]))


# =============== part 2 ===============

d = np.ones((h,w)) * (h*w)
q = col.deque()

d[sx, sy] = 0
q.appendleft((sx,sy))

# add all the other starting positions
for i, r in enumerate(data):
  for j, c in enumerate(r):
    if data[i][j] == 'a':
      d[i,j] = 0
      q.appendleft((i,j))

while len(q) > 0:
  x,y = q.pop()
  if (x,y) == end:
    break
  for dx, dy in [(1,0), (-1,0), (0,1), (0,-1)]:
    ox, oy = x+dx, y+dy
    if 0 <= ox < h\
      and 0 <= oy < w\
      and relief[ox, oy] <= relief[x,y] + 1\
      and d[x,y] + 1 < d[ox, oy]:
      d[ox, oy] = d[x,y] + 1
      q.appendleft((ox, oy))
      # print(ox, oy, d[ox, oy])

# print(d)
print(int(d[ex,ey]))





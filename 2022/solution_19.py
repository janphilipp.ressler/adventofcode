# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

pat = "Blueprint (\d+): Each ore robot costs (.+). Each clay robot costs (.+). Each obsidian robot costs (.+). Each geode robot costs (.+)."
m = re.compile(pat)
data = [ m.match(line).groups() for line in open(0).read().splitlines() ]

# print(data)

def parseBlueprints(t: tuple):
  dd = dict()
  for rob, s in zip(State.robotNames, t):
    d = dict()
    for spec in s.split(' and '):
      num, res = spec.split()
      d[res] = int(num)
    dd[rob] = Res(**d)
  return dd


#TODO do classes with NamedTuple
class Res(NamedTuple):
  resourceNames = ['ore', 'clay', 'obsidian', 'geode']
  # default: dict = {k: 0 for k in resourceNames}
  ore: int = 0
  clay: int = 0
  obsidian: int = 0
  geode: int = 0
  resourceValues = {'ore': 1, 'clay': 2, 'obsidian': 50, 'geode': 1000}

  # def __init__(self, d: dict):
  #   self.ore = self.clay = self.obsidian = self.geode = 0
  #   for k,v in d.items():
  #     if k in Res.default.keys():
  #       if k == 'ore': self.ore = v
  #       elif k == 'clay': self.clay = v
  #       elif k == 'obsidian': self.obsidian = v
  #       elif k == 'geode': self.geode = v
  #       else: print("What is this?:", k, v)

  # def defaultInit():
  #   return Res(Res.default)

  # def d(self):
  #   return {k:v for k,v in zip(Res.default.keys(), [self.ore, self.clay, self.obsidian, self.geode])}

  def __add__(self, o):
    sd = self._asdict()
    od = o._asdict()
    # print('self', self)
    # print('o', o)
    return Res(**{ k: sd[k] + od[k] for k in sd.keys() })

  def __sub__(self, o):
    sd = self._asdict()
    od = o._asdict()
    return Res(**{ k: sd[k] - od[k] for k in sd.keys() })

  def __mul__(self, o):
    sd = self._asdict()
    # print('in mul', sd, sd.keys())
    return Res(**{ k: sd[k] * int(o) for k in sd.keys() })
  __rmul__ = __mul__

  def __str__(self):
    # print(self.ore, 'clay', self.clay, self.geode, self.obsidian)
    return '{:>2} {:>2} {:>2} {:>2}'.format(self.ore, self.clay, self.obsidian, self.geode)

class BuildOptions(NamedTuple):
  ore: int = 0
  cla: int = 0
  obs: int = 0
  geo: int = 0

class State():
  robotNames = ['ore-robot', 'clay-robot', 'obsidian-robot', 'geode-robot']
  robotCosts: dict
  robotNums: dict
  res: Res
  # buyOptions: List[Tuple]

  def __init__(self, rc: dict,
                     rn: dict = {k: 1 if k == 'ore-robot' else 0 for k in robotNames},
                     res: Res = Res()):
    self.robotCosts = rc
    self.robotNums = rn
    self.res = res

  def buildOptions(self) -> list:
    # return all possible states for buying options.
    def affordable(t: tuple):
      rc = sum([num * costs for num, costs in zip(t, self.robotCosts.values())], start = Res())
      newres = self.res - rc
      return all(map(lambda x: x >= 0, newres._asdict().values()))

    options = set()
    start = BuildOptions()
    for numgeos in range(2):
      geostart = start._replace(geo = numgeos)
      if affordable(geostart):
        options.add(geostart)
        for numobs in range(2):
          obsstart = geostart._replace(obs = numobs)
          if affordable(obsstart):
            options.add(obsstart)
            for numcla in range(3):
              clastart = obsstart._replace(cla = numcla)
              if affordable(clastart):
                options.add(clastart)
                for numore in range(3):
                  fin = clastart._replace(ore = numore)
                  if affordable(fin):
                    options.add(fin)

    options = [(0,0,0,0), (0,0,0,1), (0,0,1,0), (0,1,0,0), (1,0,0,0)]

    ls = []
    for op in filter(affordable, options):
      rc = sum([num * costs for num, costs in zip(op, self.robotCosts.values())], start = Res())
      newState = State(self.robotCosts,
          rn = { k: v + nv for (k,v), nv in zip(self.robotNums.items(), op)},
          res = self.res - rc )
      ls.append(newState)
    return ls


  def generateRes(self):
    d = { k: self.robotNums[k+'-robot'] for k in Res.resourceNames }
    return Res(**d)

  def acceptRes(self, res: Res):
    self.res += res


  def getScore(self):
    # score = 0
    res = sum([self.robotNums[robot] * self.robotCosts[robot] for robot in State.robotNames], start = self.res)
    return sum(Res.resourceValues[r] * res._asdict()[r] for r in Res.resourceNames)# - sum(self.robotNums.values())

  def __hash__(self):
    return self.getScore()

  def __eq__(self, o):
    ls = list(self.robotNums.values()) + list(self.res._asdict().values())
    ols = list(o.robotNums.values()) + list(o.res._asdict().values())
    assert len(ls) == 8 == len(ols)
    return tuple(ls) == tuple(ols)

  # def __ne__(self, o):
  #   return not self.__eq__(o)

  def __str__(self):
    s = str(self.res)
    robs = [ f"{k:<14} {self.robotNums[k]:>2}   {v}" for k,v in self.robotCosts.items() ]
    return '\n'.join([s] + robs)



# =============== part 1 ===============

# def recursivePath(cur: State, time = 0):
#   cur.forward()
#   if time == 9 and cur.robotNums['clay-robot'] == 0:
#     return 0
#   if time == 15 and cur.robotNums['obsidian-robot'] == 0:
#     return 0
#   if time + 1 >= max_time:
#     return cur.res.geode
#   ls = cur.buildOptions()
#   return max(recursivePath(state, time + 1) for state in ls)

stateKey = lambda state: state.getScore()

result = 0
max_time = 24
for blueprint in data:
  uid, rc = int(blueprint[0]), blueprint[1:]
  start = State(parseBlueprints(rc))

  print('Start calculating', uid)
  q = [start]
  for i in range(max_time):
    # print(q)
    # print('TIME:', i + 1)
    newones = set()
    for cur in q:
      # buildOptions
      news = cur.buildOptions()
      # print('options', news)

      # generateRes
      res = cur.generateRes()
      # if i == 18:
      #   print(res)

      # give all New States its res
      # print('nowupdate')
      for state in news:
        state.acceptRes(res)
        newones.add(state)
        # newq.append(state)
    # print(len(q))
    # print(len(newones))
    q = sorted(newones, key=stateKey, reverse=True)[:150] 
    first, last = q[0], q[-1]
    # print(first, '\n', last)
    # print(first.getScore(), last.getScore())
  # print(*newones, sep = '\n')
  print(first.res.geode)
  result += uid * first.res.geode
  print(result)


  




# =============== part 2 ===============






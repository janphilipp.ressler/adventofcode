# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read().strip().splitlines()

# print(len(data), len(data[0]))

# =============== part 1 ===============

height = len(data)
width = len(data[0])

grid = np.zeros((height, width))
for i in range(height):
  left = right = -1
  for j in range(width):
    tree = int(data[i][j])
    grid[i][j] += (1 if tree > left else 0)
    left = max(left, tree)
  for j in reversed(range(width)):
    tree = int(data[i][j])
    grid[i][j] += (1 if tree > right else 0)
    right = max(right, tree)

for i in range(width):
  bottom = above = -1
  for j in range(height):
    tree = int(data[j][i])
    grid[j][i] += 1 if tree > above else 0
    above = max(above, tree)
  for j in reversed(range(height)):
    tree = int(data[j][i])
    grid[j][i] += 1 if tree > bottom else 0
    bottom = max(bottom, tree)

print(np.count_nonzero(grid))


# =============== part 2 ===============

def calcscore(i,j):
  score = 1
  for di, dj in [(-1,0), (1,0), (0,-1), (0,1)]:
    dist = calcdist(di, dj, i, j)
    # print(i,j, dist, di, dj)
    score *= dist
  return score

def calcdist(di, dj, i, j):
  tree = int(data[i][j])
  oi = i + di
  oj = j + dj
  c = 0
  while 0 <= oi < height and 0 <= oj < width:
    c += 1
    if tree <= int(data[oi][oj]):
      break
    oi += di
    oj += dj
  return c


m = 0
for i in range(height):
  for j in range(width):
    s = calcscore(i,j)
    m = max(m, s)

print(m)




# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
import pprint

# =============== preparation ===============

data = open(0).read().split('$ ')


# =============== part 1 ===============


def parsedir(l):
  return l.split()[1]

def parsefile(l):
  # print(l.split())
  size, name = l.split()
  return name, int(size)

d = 0
# cur = '/'
A = a = dict()
a['/'] = dict()
for token in data[1:]:
  if token.startswith('ls'):
    cs = token.strip().splitlines()[1:]
    for c in cs:
      if c.startswith('dir'):
        n = parsedir(c)
        a[n] = dict()
        a[n]['prev'] = a
      else:
        n,s = parsefile(c)
        a[n] = s
  elif token.startswith('cd'):
    j = token.split()[1]
    if j == '..':
      a = a['prev']
    else:
      a = a[j]
  else:
    print('ERROR')


ls = []
ss = []
def cs(d: dict):
  if '###' in d.keys():
    return d['###']
  ls.append(d)
  s = 0
  for k,v in d.items():
    if k == 'prev':
      continue
    elif type(v) == int:
      s += v
    elif type(v) == dict:
      s += cs(v)
    else:
      print('ERROR', k, v)
  d['###'] = s
  ss.append(s)
  return s

cs(A)
# pprint.pprint(A)

# s = sum([d['###'] for d in ls if d['###'] <= 100000])
# print(s)
s = sum([e for e in ss if e <= 100000])
print(s)


# =============== part 2 ===============

total = 70000000
used = A['###']
unused = total - used
needed = 30000000 - unused

ts = [ (i, e - needed) for i, e in enumerate(ss) ]
# print(ts)
ts = list(filter( lambda t: t[1] >= 0, ts ))
ts.sort(key = lambda t: t[1])

idx = ts[0][0]
print(ss[idx])






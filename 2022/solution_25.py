# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().splitlines()

# =============== part 1 ===============

highestnumbers, lowestnumbers = [0], [0]

h, l = 0, 0
for i in range(20):
  h += 2 * 5 ** i
  l -= 2 * 5 ** i
  highestnumbers.append(h)
  lowestnumbers.append(l)


def SNAFUtoDEC(s: str) -> int:
  plus, minus = '0', '0'
  for c in s:
    if c in '210':
      plus += c
      minus += '0'
    elif c in '-=':
      plus += '0'
      if c == '-':
        minus += '1'
      else:
        minus += '2'
    else:
      print('ERROR')
  # print(plus, minus)
  return int(plus, 5) - int(minus, 5)


def place(p: int, i: int, n: int) -> int:
  maxk = -2
  for k in [-1, 0, 1, 2]:
    if abs((n + k * 5 ** p) - i) < abs((n + maxk * 5 ** p) - i):
      maxk = k
  return maxk

encoding = { -2: '=', -1: '-', 0: '0', 1: '1', 2: '2' }
def DECtoSNAFU(i: int) -> str:
  l = 0
  while highestnumbers[l] < i:
    l += 1

  n = 0
  s = ''
  for p in reversed(range(l)):
    k = place(p, i, n)
    s += encoding[k]
    n += k * 5 ** p
  return s


def part1():
  return DECtoSNAFU(sum(SNAFUtoDEC(s) for s in data))

print(part1())


# =============== part 2 ===============

def part2():
  # need 3 more stars from previous tasks
  pass

part2()



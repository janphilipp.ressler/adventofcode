# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *
from pprint import pprint

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = open(0).read().strip()

# uno = ['..####.']
# due = ['...#...',
#        '..###..',
#        '...#...']
# tre = ['....#..',
#        '....#..',
#        '..###..']
# qua = ['..#....',
#        '..#....',
#        '..#....',
#        '..#....']
# cin = ['..##...',
#        '..##...']

uno = ( 2+0j, 3+0j, 4+0j, 5+0j )
due = ( 3+0j, 2+1j, 4+1j, 3+2j, 3+1j )
tre = ( 2+0j, 4+0j, 3+0j, 4+1j, 4+2j )
qua = ( 2+0j, 2+1j, 2+2j, 2+3j )
cin = ( 2+0j, 3+0j, 2+1j, 3+1j )

forms = [uno, due, tre, qua, cin]

def formLoop():
  while True:
    for f in forms:
      yield f

def gasLoop():
  while True:
    for c in data:
      yield c == '<'

# =============== part 1 ===============

chamber = [[True] * 7]
for i in range(4000):
  chamber.append([False] * 7)

def printChamber():
  for r in chamber:
    for b in r:
      print('#' if b else ' ', end = '')
    print()

def checkBounds(f, move):
  return not all( 0 <= pos.real + move.real < 7 for pos in f)

def checkCollisions(f: List[complex], move: complex):
  for pos in f:
    pos += move
    if chamber[int(pos.imag)][int(pos.real)]:
      return True
  return False

def updateForm(f, move):
  return tuple( pos + move for pos in f)

# gas = (c == '<' for c in data[:10])
gas = gasLoop()
looper = formLoop()

# heights = [0] * 7
num_rocks = 2022
max_height = 0
for i in range(num_rocks):
  f = next(looper)
  f = updateForm(f, (4+max_height)* 1j ) 

  while True:
    # side move
    # check for boundaries and collisions
    # update block location in case
    pushleft = next(gas)
    move = -1 if pushleft else 1
    if checkBounds(f, move) or checkCollisions(f, move):
      pass
    else:
      f = updateForm(f,move)
    # down move
    # check for collisions - break in case
    move = -1j
    if checkCollisions(f, move):
      break
    else:
      f = updateForm(f,move)
  for pos in f:
    if pos.imag > max_height:
      max_height = pos.imag
    chamber[int(pos.imag)][int(pos.real)] = True

# printChamber()
 
print(int(max_height))


# =============== part 2 ===============

num_rocks = 1000000000000





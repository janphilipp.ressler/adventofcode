# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *
from pprint import pprint

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

board, route = open(0).read().split('\n\n')

board = board.splitlines()
longestlinelen = max(map(len, board))
tablelen = gcd(len(board), longestlinelen)


chapat = re.compile('[LR]')
forwards, _ = chapat.subn(' ', route)
numpat = re.compile('\d+')
rotations, _ = numpat.subn(' ', route)

forwards = list(map(int, forwards.strip().split()))
rotations = rotations.split()
rotationsGen = (r for r in rotations)
# print(forwards)
# print(rotations)



tables = { (r,c): set() for r,c in it.product(range(len(board) // tablelen), range(longestlinelen // tablelen)) }

for i, row in enumerate(board):
  for j, col in enumerate(row):
    if col == '#':
      ti, ii = divmod(i, tablelen)
      tj, jj = divmod(j, tablelen)
      tables[(ti,tj)].add(( jj + ii * 1j))
      # print(ti, tj, ii, jj)

# print(tables.keys())
# pprint(tables)
# print('tablelen', tablelen)



# =============== part 1 ===============

tbr = [ k for k in tables.keys() if tables[k] == set() ]
for k in tbr:
  tables.pop(k)
# print(tables.keys())


def tableboarder(ID, pos):
  r,c = ID
  if 0 > pos.real:
    ls = list(reversed(list(filter(lambda ids: ids[0] == r, tables.keys()))))
    index = ls.index(ID)
    lls = ls[index+1:] + ls[:index+1]
    ID = lls[0]
    pos = complex(tablelen - 1, pos.imag)
    # print(pos, ID, ls, lls)
  elif pos.real >= tablelen:
    ls = list(filter(lambda ids: ids[0] == r, tables.keys()))
    index = ls.index(ID)
    lls = ls[index+1:] + ls[:index+1]
    ID = lls[0]
    pos = complex(0, pos.imag)
    # print(pos, ID, ls, lls)
  elif 0 > pos.imag:
    ls = list(reversed(list(filter(lambda ids: ids[1] == c, tables.keys()))))
    index = ls.index(ID)
    lls = ls[index+1:] + ls[:index+1]
    ID = lls[0]
    pos = complex(pos.real, tablelen - 1)
    # print(pos, ID, ls, lls)
  elif pos.imag >= tablelen:
    ls = list(filter(lambda ids: ids[1] == c, tables.keys()))
    index = ls.index(ID)
    lls = ls[index+1:] + ls[:index+1]
    ID = lls[0]
    pos = complex(pos.real, 0)
    # print(pos, ID, ls, lls)
  else:
    print('ERROR')
  return ID, pos


# following instructions
tableID, pos = start = (list(tables.keys())[0], 0+0j)
di = 0
for go in forwards:
  step = 1 if di == 0 else 1j if di == 1 else -1 if di == 2 else -1j
  for _ in range(go):
    # print(tableID, pos, '->', end = '')
    nID = tableID
    n = pos + step
    if not (0 <= n.real < tablelen and 0 <= n.imag < tablelen):
      nID, n = tableboarder(nID, n)
    if n in tables[nID]:
      # print()
      break
    tableID = nID
    pos = n
    # print('->', tableID, pos)
  # dir change
  match next(rotationsGen, None):
    case 'R':
      di += 1
    case 'L':
      di -= 1
    case None:
      break
  di %= 4


# calculating password
rt, ct = tableID
r, c = int(pos.imag), int(pos.real)

row = rt * tablelen + r + 1
col = ct * tablelen + c + 1
print(1000 * row + 4 * col + di)



# =============== part 2 ===============






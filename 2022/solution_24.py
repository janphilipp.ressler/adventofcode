# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

lines = open(0).read().splitlines()

walls, dos, ups, ris, les = set(), list(), list(), list(), list()
for i, row in enumerate(lines):
  for j, char in enumerate(row):
    coord = complex(j,i)
    match char:
      case '#': walls.add(coord)
      case 'v': dos.append(coord)
      case '^': ups.append(coord)
      case '>': ris.append(coord)
      case '<': les.append(coord)

topwall, botwall, lefwall, rigwall = 0, len(lines) - 1, 0, len(lines[0]) - 1
do, up, ri, le = 1j, -1j, 1, -1

# =============== part 1 ===============

def findway(start, end, ups, dos, les, ris, returnSets = False):
  poss = {start}
  time = 0
  while end not in poss:
    # calc new blizzards
    # ups = [ b + up for b in ups ]
    ups = [ b + up if b.imag > topwall + 1 else b + do * (botwall - topwall - 2) for b in ups ]
    dos = [ b + do if b.imag < botwall - 1 else b + up * (botwall - topwall - 2) for b in dos ]
    les = [ b + le if b.real > lefwall + 1 else b + ri * (rigwall - lefwall - 2) for b in les ]
    ris = [ b + ri if b.real < rigwall - 1 else b + le * (rigwall - lefwall - 2) for b in ris ]
    blis = set(ups) | set(dos) | set(les) | set(ris)

    # calc possible new poss
    news = set()
    for pos in poss:
      news.update( [ pos, pos + do, pos + up, pos + ri, pos + le ] )
    
    poss = set(filter(lambda pos: topwall <= pos.imag <= botwall
                              and lefwall <= pos.real <= rigwall
                              and pos not in walls
                              and pos not in blis
                              , news))
    time += 1
  if returnSets:
    return time, ups, dos, les, ris
  return time

def part1(ups, dos, les, ris):
  start = complex(1,0)
  end = complex(rigwall - 1, botwall)

  print(findway(start, end, ups, dos, les, ris))

part1(ups, dos, les, ris)


# =============== part 2 ===============

def part2(ups, dos, les, ris):
  start = complex(1,0)
  end = complex(rigwall - 1, botwall)

  a, ups, dos, les, ris = findway(start, end, ups, dos, les, ris, returnSets = True)
  b, ups, dos, les, ris = findway(end, start, ups, dos, les, ris, returnSets = True)
  c, ups, dos, les, ris = findway(start, end, ups, dos, les, ris, returnSets = True)

  print(a + b + c)

part2(ups, dos, les, ris)



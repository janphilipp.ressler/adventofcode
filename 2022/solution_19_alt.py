# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

pat = "Blueprint (\d+): Each ore robot costs (.+). Each clay robot costs (.+). Each obsidian robot costs (.+). Each geode robot costs (.+)."
m = re.compile(pat)
data = [ m.match(line).groups() for line in open(0).read().splitlines() ]
# print(data)

infty = 10000
table = {'ore': 0, 'clay': 1, 'obsidian': 2, 'geode': 3 }
def parseBlueprints(t: tuple):
  robotList = []
  maxCost = [0] * 4
  for s in t[1:]:
    rob = [0] * 4
    for spec in s.split(' and '):
      num, res = spec.split()
      resid = table[res]
      rob[resid] = int(num)
      maxCost[resid] = max(maxCost[resid], rob[resid])
    robotList.append(tuple(rob))
  maxCost[-1] = infty
  return int(t[0]), robotList, tuple(maxCost)


# =============== part 1 ===============

resourceValues = [1, 3, 50, 1000]
def score(state: tuple):
  allres = state[-1]
  return sum( multiplier * res for multiplier, res in zip(resourceValues, allres) )


def simulateBP(robotCosts, maxCost, time = 24, cutoff = 200):
  res = [0, 0, 0, 0]
  robots = [1, 0, 0, 0]
  allres = res
  startState = (res, robots, allres)

  states = [startState]
  while time:
    newStates = list()
    # print(states)
    for state in states:
      res, robots, allres = state
      # generate res
      newres = robots.copy()

      # build new states
      childres = [ cr + nr for cr, nr in zip(res, newres) ]
      childallres = [ ar + nr for ar, nr in zip(allres, newres) ]
      newStates.append( (childres, robots.copy(), childallres) )
      
      for rid in range(len(robotCosts)):
        if robots[rid] + 1 > maxCost[rid]:
          continue
        childrobs = robots.copy()
        childrobs[rid] += 1
        childres = res.copy()
        for resid, resCost in enumerate(robotCosts[rid]):
          childres[resid] -= resCost
          if childres[resid] < 0:
            break
        else:
          # try
          # 1 - convert to tuple and throw in set
          # 2 - use lists only
          childres = [ cr + nr for cr, nr in zip(childres, newres) ]
          childallres = [ ar + nr for ar, nr in zip(allres, newres) ]
          newStates.append( (childres, childrobs, childallres) )

    # sort States
    states = sorted(newStates, key=score, reverse=True)[:cutoff]
    bestState = states[0]
    # print(bestState)
    time -= 1
  gs = bestState[-1][-1]
  return gs


def part1():
  result = 0
  for bp in data:
    uid, l, maxres = parseBlueprints(bp)
    gs = simulateBP(l, maxres)
    result += uid * gs
  return result

print(part1())
  

# =============== part 2 ===============

def part2():
  results = []
  for bp in data[:3]:
    uid, l, maxres = parseBlueprints(bp)
    gs = simulateBP(l, maxres, time = 32, cutoff = 400)
    results.append(gs)
  # print(results)
  return ft.reduce( int.__mul__, results, 1)

print(part2())



# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = [ (i, int(v)) for i, v in enumerate(open(0).read().splitlines()) ]
l = len(data)

# =============== part 1 ===============

def mix(wl: list, origData = data):
  for i, v in origData:
    idx = wl.index((i,v))
    oldv = v
    v = v % (l-1)
    if idx + v >= l:
      v = v - l + 1
    elif idx + v < 0:
      v = l - 1 + v
    if v > 0:
      wl = wl[:idx] + wl[idx+1:idx+v+1] + [(i,oldv)] + wl[idx+v+1:]
    elif v < 0:
      wl = wl[:idx + v] + [(i,oldv)] + wl[idx+v:idx] + wl[idx+1:]
    # print(len(wl))
  return wl


def calcGrove(wl: list):
  ls = []
  for i, (_, v) in enumerate(wl):
    if v == 0:
      ls.append((i,v))
  # print(ls)

  idx = ls[0][0]
  a = wl[(idx + 1000) % l][1]
  b = wl[(idx + 2000) % l][1]
  c = wl[(idx + 3000) % l][1]
  # print(a,b,c)
  return a + b + c

pl = mix(data.copy())
print(calcGrove(pl))

# =============== part 2 ===============

secretnumber = 811589153
newdata = [(i, v * secretnumber) for i, v in data]

pl = newdata.copy()
for i in range(10):
  # print('round', i+1)
  pl = mix(pl, origData = newdata)

print(calcGrove(pl))






# =============== imports ===============
import sys
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *


# =============== preparation ===============

inp = sys.stdin
data = inp.read().splitlines()



# =============== part 1 ===============
tab = {'A': 1, 'X': 1, 'B': 2, 'Y': 2, 'C': 3, 'Z': 3}
data = [e.split() for e in data]
d = [ (tab[a], tab[b]) for a, b in data]

def points(a, b):
  if a == b:
    return a + 3
  if a == 1:
    if b == 2:
      return b + 6
    else:
      return b
  if a == 2:
    if b == 3:
      return b + 6
    else:
      return b
  if a == 3:
    if b == 1:
      return b + 6
    else:
      return b

r = [points(a,b) for a,b in d]
print(sum(r))


# =============== part 2 ===============


def points2(a, b):
  if b == 1:
    if a == 1:
      return 3
    if a == 2:
      return 1
    if a == 3:
      return 2
  if b == 2:
    return a + 3
  if b == 3:
    if a == 1:
      return 2 + 6
    if a == 2:
      return 3 + 6
    if a == 3:
      return 1 + 6

r = [points2(a,b) for a,b in d]
print(sum(r))




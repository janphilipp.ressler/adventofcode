# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

from pprint import pprint

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])


# =============== preparation ===============

pat = "^Valve (.+) has flow rate=(\d+); tunnels? leads? to valves? (.*)$"
m = re.compile(pat)

data = [ (i,) + m.match(line).groups() for i, line in enumerate(open(0).read().splitlines()) ]
data = [ {'uid': k[1], 'rate': int(k[2]), 'to': k[3].split(', ') } for k in data ]
# pprint(data)
numvalves = len(data)

mapping = { ele['uid']: i for i, ele in enumerate(data) }


dm = distMatrix = np.ones((numvalves, numvalves), dtype = int) * 1001
for i in range(numvalves):
  dm[i,i] = 0
for ele in data:
  for adj in ele['to']:
    dm[mapping[ele['uid']], mapping[adj]] = 1
for k in range(numvalves):
  for i in range(numvalves):
    for j in range(numvalves):
      if dm[i][j] > dm[i][k] + dm[k][j]:
        dm[i][j] = dm[i][k] + dm[k][j]


mask = [ ele['rate'] != 0 for ele in data ]
mask[mapping['AA']] = True

# idm = dm[mask, mask] # why doesn't this work?
idm = dm[mask]
idm = idm[:, mask]
# print(idm)

idata = [ valve for valve, important in zip(data, mask) if important ]

max_time = 30
releases = np.zeros((max_time, len(idata)), dtype = int)
for opentime in range(max_time):
  for i, valve in enumerate(idata):
    releases[opentime, i] = (max_time - opentime) * valve['rate']
# print(releases)

# print('Precalculations finished')

# =============== part 1 ===============

mapping = { ele['uid']: i for i, ele in enumerate(idata) }
s = set(range(len(idata)))

def recursivePath(cur: int, vis: set, time: int):
  m = 0
  for o in s - vis:
    dist = idm[cur, o]
    needed = time+dist+1
    if needed >= max_time:
      continue
    reward = releases[min(needed, 29), o]
    bestres = reward + recursivePath(o, vis | {o}, needed)
    m = bestres if bestres > m else m
  return m

start = mapping['AA']
res = recursivePath(start, {start}, 0)
print(res)

# =============== part 2 ===============

pathlist = []
pathtree = dict()
max_time = 26

s.remove(start)
def pathCreator(cur: int, ls: list, time: int, tree: dict):
  vis = frozenset(ls)
  for o in s - vis:
    needed = time + idm[cur, o] + 1
    if needed >= max_time:
      continue
    nls = ls + [o]
    pathlist.append(nls)
    tree[o] = dict()
    pathCreator(o, nls, needed, tree[o])

pathCreator(start, [], 0, pathtree)
# print(len(pathlist))


def calcRelease(path):
  time = 4
  cur = start
  release = 0
  for valve in path:
    time += idm[cur,valve] + 1
    release += releases[time, valve]
    cur = valve
  return release

def calcNonIntersectingSubtreeRelease(path: set, tree: dict, time: int, cur: int):
  rewards = [0]
  for k, subtree in tree.items():
    if k not in path:
      newtime = time + idm[cur, k] + 1
      release = releases[newtime, k]
      subrelease = calcNonIntersectingSubtreeRelease(path, subtree, newtime, k)
      rewards.append(release + subrelease)
  return max(rewards)

maxreward = max(calcRelease(path) for path in pathlist)
# print('maxreward:', maxreward)

m = 0
for i, path in enumerate(pathlist):
  # if i % 1000 == 0:
  #   print('round', i)
  reward = calcRelease(path)
  if reward < m - maxreward:
    # at this point we can be sure we would have found that reward as otherreward wehen this reward is the maxreward.
    continue
  otherreward = calcNonIntersectingSubtreeRelease(set(path), pathtree, 4, start)
  if reward + otherreward > m:
    m = reward + otherreward
    # print(m)

print(m)



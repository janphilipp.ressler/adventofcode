# =============== imports ===============
import sys
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *

# =============== preparation ===============

data = open(0).read().splitlines()

pat = r"(\d+)-(\d+),(\d+)-(\d+)"
m = re.compile(pat)
inp = [ list(map(int, m.match(line).groups())) for line in data ]

# =============== part 1 ===============

f = lambda a, b, x, y: (a >= x and b <= y) or (a <= x and b >= y)
includes = [ f(*t) for t in inp ]
print(sum(includes))

# =============== part 2 ===============

g = lambda a, b, x, y: (x <= a <= y) or (x <= b <= y) or f(a,b,x,y)
overlaps = [ g(*t) for t in inp ]
print(sum(overlaps))


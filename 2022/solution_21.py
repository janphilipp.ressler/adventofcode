# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *
from typing import *

# =============== defaults ===============
c = lambda s: complex(s.replace(',', '+') + 'j')
# [a, b, c, d] -> [(a,b), (b,c), (c,d)]
# zip(l, l[1:])

# =============== preparation ===============

data = { k: val for k, val in [ line.split(': ') for line in open(0).read().splitlines()] }

# =============== part 1 ===============

def rec(s: str):
  if ' ' not in data[s]: return int(data[s])
  a, op, b = data[s].split()
  return eval(f'{rec(a)} {op} {rec(b)}')

print(int(rec('root')))

# =============== part 2 ===============

def recfind(s: str, tofind = 'humn'):
  if s == tofind:
    return True
  if ' ' not in data[s]:
    return False
  if tofind in data[s]:
    return True
  else:
    a, _, b = data[s].split()
    return recfind(a) or recfind(b)


def recsolve(s: str, supposed: int):
  # print(s, data[s])
  if s == 'humn':
    return supposed

  a, op, b = data[s].split()
  if op == '+':
    other, tosearch = (rec(b), a) if recfind(a) else (rec(a), b)
    return recsolve(tosearch, supposed - other)
  elif op == '*':
    other, tosearch = (rec(b), a) if recfind(a) else (rec(a), b)
    return recsolve(tosearch, supposed // other)
  elif op == '-':
    if recfind(a):
      # supposed = rec(a) - b
      return recsolve(a, supposed + rec(b))
    else:
      # supposed = a - rec(b)
      return recsolve(b, rec(a) - supposed)
  elif op == '/':
    if recfind(a):
      # supposed = rec(a) / b
      return recsolve(a, supposed * rec(b))
    else:
      # supposed = a / rec(b)
      return recsolve(b, rec(a) // supposed)
  else:
    print('Error')


a, _, b = data['root'].split()
print(int(recsolve(a, rec(b))))



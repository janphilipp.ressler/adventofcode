input = open("data_07.in").read().strip()
lines = input.split("\n")[::-1]
linesiter = iter(lines)

def parseFolderOrFile():
 cf = {}
 print(type(cf))
 while True:
  line = next(linesiter)
  print(line)
  linesplit = line.split(" ")
  if line[0].isdigit(): # 152092 gwwh.ttt
   cf[linesplit[1]] = int(linesplit[0])
  elif line[0] == "d": # dir jhgds
   pass
  elif line == "$ ls":
   pass
  elif line == "$ cd ..":
   key, value = parseFolderOrFile()
   cf[key] = value
  elif line.startswith("$ cd"):
   return (linesplit[-1], cf)
  else:
   print("oh nein..", line)

key, folders = parseFolderOrFile()

def folderSize(cf):
 summe = 0
 for value in cf.values():
  if isinstance(value, dict):
   summe += folderSize(value)
  else:
   summe += value 
 return summe

summe2 = 0
def folderSize2(cf):
 global summe2
 cfSize = folderSize(cf)
 if cfSize <= 100000:
  summe2 += cfSize
 for value in cf.values():
  if isinstance(value, dict):
   folderSize2(value)

folderSize2(folders)
# print(summe)
print(summe2)


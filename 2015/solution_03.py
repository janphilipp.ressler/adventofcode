# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col




# =============== preparation ===============
data = open(0).read()


# =============== part 1 ===============

def f(l):
  x, y = 0,0

  d = col.defaultdict(int)
  d[(0,0)] = 1

  for c in l:
    match c:
      case 'v': y += 1
      case '^': y -= 1
      case '>': x += 1
      case '<': x -= 1

    d[(x,y)] += 1

  return set(d.keys())


print(len(f(data)))

# =============== part 2 ===============

print(len(f(data[::2]) | f(data[1::2])))





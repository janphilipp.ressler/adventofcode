# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col


# =============== preparation ===============
data = open(0).read()

# =============== part 1 ===============


u = data.count('(')
l = data.count(')')

print(u - l )


# =============== part 2 ===============

count = 0

for i, c in enumerate(data):
  if c == '(':
    count += 1
  else:
    count -= 1

  if count == -1:
    print(i)
    break






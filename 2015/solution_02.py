# =============== imports ===============
import re, copy
import numpy as np
import itertools as it
import functools as ft
import collections as col
from math import *


# =============== preparation ===============
data = open(0).read().splitlines()



# =============== part 1 ===============

r = 0
for line in data:
  a, b, c = map(int, line.split('x'))
  l = [a*b, a*c, b*c]

  r += 2* sum(l) + min(l)


print(r)





# =============== part 2 ===============


r=0
for line in data:
  a, b, c = l = list(map(int, line.split('x')))
  r += 2 * (sum(l) - max(l))
  r += prod(l)

print(r)


# Advent of Code Solutions

## Scripts

There are three scripts `start`, `solve` and `submit`.

`start`: ... (todo)
`start`: ... (todo)
`start`: ... (todo)

### Installation

The scripts use internally 'advent-of-code-data' - a python module. So you need to install that:

`pip install advent-of-code-data`.

Additionally you should set your Session Cookie to get your personalized puzzles:

`export AOC_SESSION=[...]`

You can find your session cookie like this: https://github.com/wimglenn/advent-of-code-wim/issues/1



